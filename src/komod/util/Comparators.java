package komod.util;

import java.util.Comparator;

/**
 * Komod, a lightweight DBMS written in Java.
 * Copyright (C) 2015 Komod
 * <p>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * See LICENCE.txt for more details.
 */
public class Comparators {
    public static final Comparator<Object> defaultComparator = new DefaultComparator();

    private Comparators() {}

    private static class DefaultComparator implements Comparator<Object> {
        public int compare(Object o1, Object o2) {
            if (o1 instanceof Integer) {
                if (!(o2 instanceof Integer)) {
                    throw new RuntimeException("Objects are not of the same type.");
                }
                return ((Integer) o1).compareTo((Integer) o2);
            } else if (o1 instanceof String) {
                if (!(o2 instanceof String)) {
                    throw new RuntimeException("Objects are not of the same type.");
                }
                return ((String) o1).compareTo((String) o2);
            } else {
                return -1;
//                throw new RuntimeException("Unknown type for compare. " + o1.getClass().toString());
            }
        }
    }
}
