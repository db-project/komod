package komod.util;

import komod.lang.NonTerminal;
import komod.lang.Rule;
import komod.lang.Symbol;
import komod.lang.Terminal;

import java.util.*;


/**
 * Copyright (C) 2016 Hadi
 */
public class ParseTableGenerator {
    private static ArrayList<Terminal> terminals;
    private static ArrayList<NonTerminal> nonterminals;
    private static HashMap<Symbol, HashSet<Object>> first;
    private static HashMap<NonTerminal, HashSet<Terminal>> follow;
    private static ArrayList<Rule> rules;
    private static String EPSILON  = "EPSILON" ;//todo : for generating parseTable we must define epsilon as a terminal

    public static void main(String[] args) throws Exception {
        terminals = new ArrayList<>(Arrays.asList(Terminal.values()));
        nonterminals = new ArrayList<>(Arrays.asList(NonTerminal.values()));
        rules = new ArrayList<>(Arrays.asList(Rule.values()));

        first = new HashMap<>();
        for (Terminal t : terminals) {
            HashSet<Object> arr = new HashSet<>();
            arr.add(t);
            first.put(t, arr);
        }
        for (NonTerminal nt : nonterminals) {
            first.put(nt, new HashSet<>());
        }

        for (Rule rule : rules) {
            if (rule.getRightHandSide().length == 0){
                first.get(rule.getLeftHandSide()).add(EPSILON);
            }
        }

        boolean changed;
        do {
            changed = false;
            for (Rule rule : rules) {
                HashSet<Object> firstOflhs = first.get(rule.getLeftHandSide());
                boolean eps = firstOflhs.contains(EPSILON);
                boolean addEps = true;
                firstOflhs.add(EPSILON);
                for (Symbol r : rule.getRightHandSide()) {
                    changed |= firstOflhs.addAll(first.get(r));
                    if (eps)
                        firstOflhs.add(EPSILON);
                    else firstOflhs.remove(EPSILON);

                    if (!first.get(r).contains(EPSILON)) {
                        addEps = false;
                        break;
                    }
                }
                if (addEps)
                    firstOflhs.add(EPSILON);
            }
        } while (changed);

        follow = new HashMap<>();
        for(NonTerminal nt : nonterminals){
            follow.put(nt,new HashSet<Terminal>());
        }

        for (Rule rule : rules){
            Symbol lhs = rule.getLeftHandSide();
            Symbol[] rhs = rule.getRightHandSide();
            for(int i = 0 ; i < rhs.length - 1 ; ++i){
                if (nonterminals.contains(rhs[i])){
                    HashSet<Object> arrayFirst = getFirst(rhs,i+1,rhs.length);
                    arrayFirst.remove(EPSILON);
                    for (Object item : arrayFirst)
                        follow.get(rhs[i]).add((Terminal) item);

                }
            }
        }

        changed = false;

        do {
            changed = false;
            for (Rule rule : rules){
                Symbol lhs = rule.getLeftHandSide();
                Symbol[] rhs = rule.getRightHandSide();
                for(int i = 0 ; i < rhs.length ; ++i){
                    if (nonterminals.contains(rhs[i])){
                        HashSet<Object> arrayFirst = getFirst(rhs,i+1,rhs.length);
                        if (arrayFirst.contains(EPSILON)){
                            boolean followChanged = follow.get(rhs[i]).addAll(follow.get(lhs));
                            if (followChanged == true)
                                changed = true;
                        }
                    }
                }
            }
        } while (changed);

        Rule[][] table = new Rule[nonterminals.size()][];
        for (int i = 0 ; i < table.length ; i++) {
            table[i] = new Rule[terminals.size()];
            for (int j = 0 ; j < table[i].length ; ++j)
                table[i][j] = null;
        }

        int ruleIndex = 1;
        for (Rule rule : rules){
            Symbol lhs = rule.getLeftHandSide();
            Symbol[] rhs = rule.getRightHandSide();
            HashSet<Object> firtOfRhs = getFirst(rhs, 0, rhs.length);
            Rule[] lhsRow = table[nonterminals.indexOf(lhs)];
            if (firtOfRhs.contains(EPSILON)){
                for (Symbol followTerminal : follow.get(lhs)) {
                    Rule previousRule = lhsRow[terminals.indexOf(followTerminal)];
                    if ((previousRule != null) && (previousRule != rule)){
                        throw new Exception(String.format("parsTable for NonTerminal : %s and Terminal : %s has two rule. \n %s \n %s", lhs.getName() , followTerminal.getName() , previousRule , rule));
                    }
                    lhsRow[terminals.indexOf(followTerminal)] = rule;
                }
            }
            firtOfRhs.remove(EPSILON);
            for (Object item : firtOfRhs){
                Rule previousRule = lhsRow[terminals.indexOf((Terminal) item)];
                if ((previousRule != null) && (previousRule != rule)){
                    throw new Exception(String.format("parsTable for NonTerminal : %s and Terminal : %s has two rule. \n %s \n %s", lhs.getName() , ((Terminal) item).getName() , previousRule , rule));
                }
                lhsRow[terminals.indexOf(item)] = rule;
            }
            ++ ruleIndex;
        }

        System.out.print("{\n");
        for (int i = 0 ; i < table.length ; i++){
            System.out.println("\t\t\t//" + nonterminals.get(i).getName());
            System.out.print("\t\t\t{");
            for (int j = 0 ; j < table[i].length ; ++j){
                if ((j == 35) || (j == 40)){
                    System.out.print("\n\t\t\t\t\t");
                }
                System.out.print("/*" + terminals.get(j).getName() + "*/ ");
                if (table[i][j] == null){
                    System.out.print("null");
                }else {
                    System.out.print("Rule." + table[i][j].name());
                }
                if (j == table[i].length - 1){
                    System.out.print("}");
                    if (i == table.length - 1)
                        System.out.print("\n");
                    else System.out.print(",\n");
                }else
                    System.out.print(", ");
            }
        }
        System.out.print("}");


    }

    private static HashSet<Object> getFirst(Symbol[] tnt ,int beginIndex ,int endIndex ){
        HashSet<Object> firstResult = new HashSet<>();
        if ((tnt == null) || (tnt.length == 0) || (endIndex <= beginIndex)) {
            firstResult.add(EPSILON);
            return firstResult;
        }

        for (int i = beginIndex ; i < endIndex ; ++i){
            HashSet<Object> tnt_first = first.get(tnt[i]);
            firstResult.addAll(tnt_first);
            if (!tnt_first.contains(EPSILON)){
                firstResult.remove(EPSILON);
                break;
            }
        }
        return firstResult;
    }

}
