package komod.query.computation;

import java.util.ArrayList;
import java.util.Map;

/**
 * Komod, a lightweight DBMS written in Java.
 * Copyright (C) 2015 Komod
 * <p>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * See LICENCE.txt for more details.
 */
public class AvgAggregation implements Aggregation {
    private String fieldName;

    @Override
    public String getFieldName() {
        return fieldName;
    }

    @Override
    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public AvgAggregation(String fieldName) {
        this.fieldName = fieldName;
    }

    @Override
    public Object evaluate(Map<String, Object> assignments) {
        Object value = assignments.get(fieldName);
        if (value instanceof ArrayList<?>) {
            ArrayList<Object> arrayList = (ArrayList<Object>) value;
            if (arrayList.size() > 0) {
                Integer sum = 0;
                for (Object object : arrayList) {
                    if (object instanceof Integer) {
                        Integer i = ((Integer) object);
                        sum = sum+i;
                    } else {
                        return null;
                    }
                }
                return sum/arrayList.size();
            } else {
                return null;
            }
        } else {
            return null;
        }
    }
}
