package komod.query.computation;

import java.util.ArrayList;
import java.util.Map;

/**
 * Komod, a lightweight DBMS written in Java.
 * Copyright (C) 2015 Komod
 * <p>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * See LICENCE.txt for more details.
 */
public class MinAggregation implements Aggregation {
    private String fieldName;

    @Override
    public String getFieldName() {
        return fieldName;
    }

    @Override
    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public MinAggregation(String fieldName) {
        this.fieldName = fieldName;
    }

    @Override
    public Object evaluate(Map<String, Object> assignments) {
        Object value = assignments.get(fieldName);
        if (value instanceof ArrayList<?>) {
            ArrayList<Object> arrayList = (ArrayList<Object>) value;
            if (arrayList.size() > 0) {
                if (arrayList.get(0) instanceof Integer) {
                    return IntegerMin(arrayList);
                } else if (arrayList.get(0) instanceof String) {
                    return StringMin(arrayList);
                }
            }
        }
        return null;
    }

    Integer IntegerMin(ArrayList<Object> arrayList) {
        Integer ans = Integer.MAX_VALUE;
        for (Object object : arrayList) {
            if (object instanceof Integer) {
                Integer i = (Integer)object;
                ans = Integer.min(ans,i);
            } else {
                return null;
            }
        }
        return ans;
    }

    String StringMin(ArrayList<Object> arrayList) {
        String ans = ((String)arrayList.get(0));
        for (Object object : arrayList) {
            if (object instanceof String) {
                String s = (String)object;
                if (s.compareTo(ans) < 0) { // s < ans
                    ans = s;
                }
            } else {
                return null;
            }
        }
        return ans;
    }
}
