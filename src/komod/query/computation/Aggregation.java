package komod.query.computation;

/**
 * Created by keivan on 2/5/16.
 */
public interface Aggregation extends Computable {
    void setFieldName(String fieldName);
    String getFieldName();
}
