package komod.query.computation;

import java.util.Map;

/**
 * Komod, a lightweight DBMS written in Java.
 * Copyright (C) 2015 Komod
 * <p>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * See LICENCE.txt for more details.
 */
public class FieldValue implements Computable {
    private String fieldName;

    public FieldValue(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getFieldName() {
        return this.fieldName;
    }

    public void setFieldName(String fieldName) {this.fieldName = fieldName;}

    @Override
    public Object evaluate(Map<String, Object> assignments) {
        if (!assignments.keySet().contains(fieldName)) {
            throw new RuntimeException("Field does not exists: " + fieldName);
        }
        return assignments.get(fieldName);
    }
}
