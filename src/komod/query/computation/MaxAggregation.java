package komod.query.computation;

import java.util.ArrayList;
import java.util.Map;

/**
 * Komod, a lightweight DBMS written in Java.
 * Copyright (C) 2015 Komod
 * <p>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * See LICENCE.txt for more details.
 */
public class MaxAggregation implements Aggregation {
    private String fieldName;

    @Override
    public String getFieldName() {
        return fieldName;
    }

    @Override
    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public MaxAggregation(String fieldName) {
        this.fieldName = fieldName;
    }

    @Override
    public Object evaluate(Map<String, Object> assignments) {
        //System.err.println(" in evaluation fieldName = " + fieldName);
        Object value = assignments.get(fieldName);
        //System.err.println(" in evaluation value = " + value);
        if (value instanceof ArrayList<?>) {
           // System.err.println("it's ok a array");
            ArrayList<Object> arrayList = (ArrayList<Object>) value;
            if (arrayList.size() > 0) {
                //System.err.println("Size of array is : " + arrayList.size());
                if (arrayList.get(0) instanceof Integer) {
                   // System.err.println("arrayList[0] = " + arrayList.get(0));
                   // System.err.println("return result = " + IntegerMax(arrayList));
                    return IntegerMax(arrayList);
                } else if (arrayList.get(0) instanceof String) {
                    return StringMax(arrayList);
                }
            }
        }
        return null;
    }

    Integer IntegerMax(ArrayList<Object> arrayList) {
        Integer ans = Integer.MIN_VALUE;
        for (Object object : arrayList) {
            if (object instanceof Integer) {
                Integer i = (Integer)object;
                ans = Integer.max(ans,i);
            } else {
                return null;
            }
        }
        return ans;
    }

    String StringMax(ArrayList<Object> arrayList) {
        String ans = ((String)arrayList.get(0));
        for (Object object : arrayList) {
            if (object instanceof String) {
                String s = (String)object;
                if (ans.compareTo(s) < 0) { // ans < s
                    ans = s;
                }
            } else {
                return null;
            }
        }
        return ans;
    }
}
