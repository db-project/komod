package komod.query.computation;

/**
 * Komod, a lightweight DBMS written in Java.
 * Copyright (C) 2015 Komod
 * <p>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * See LICENCE.txt for more details.
 */
public interface BinaryComputation extends Computable {
    void setLHS(Computable c);
    void setRHS(Computable c);
    Computable getLHS();
    Computable getRHS();
}
