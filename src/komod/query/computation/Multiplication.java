package komod.query.computation;

import java.util.Map;

/**
 * Komod, a lightweight DBMS written in Java.
 * Copyright (C) 2015 Komod
 * <p>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * See LICENCE.txt for more details.
 */
public class Multiplication implements BinaryComputation {
    private Computable computeValue1, computeValue2;

    @Override
    public void setLHS(Computable c) {
        this.computeValue1 = c;
    }

    @Override
    public void setRHS(Computable c) {
        this.computeValue2 = c;
    }

    @Override
    public Computable getLHS() {
        return computeValue1;
    }

    @Override
    public Computable getRHS() {
        return computeValue2;
    }

    @Override
    public Object evaluate(Map<String, Object> assignments) {
        Object leftObjectValue = computeValue1.evaluate(assignments);
        Object rightObjectValue = computeValue2.evaluate(assignments);
        // todo: check class of leftObjectValue & rightObjectValue and check if they could be multiplied together or not, throw appropriate runtime exception if needed
        if (leftObjectValue == null || rightObjectValue == null) {
            throw new RuntimeException("leftObjectValue or rightObjectValue in division is null");
        }

        if (leftObjectValue instanceof  String) {
            throw  new RuntimeException("leftObjectValue is string in multiplication");
        } else if (leftObjectValue instanceof  Integer) {
            Integer leftValue = (Integer)leftObjectValue;
            if (rightObjectValue instanceof  String) {
                throw  new RuntimeException("rightObjectValue is string in multiplication");
            } else if (rightObjectValue instanceof  Integer) {
                Integer rightValue = (Integer)rightObjectValue;
                return leftValue*rightValue;
            }
        }
        throw new RuntimeException("leftvalue or rightvalue in multiplication is not a {String,Integer}");
    }
}
