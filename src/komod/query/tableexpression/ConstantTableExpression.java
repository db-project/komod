package komod.query.tableexpression;

import komod.db.*;
import komod.query.SelectFrom;
import komod.query.computation.ConstantValue;
import komod.query.computation.FieldValue;
import komod.query.condition.And;
import komod.query.condition.Equal;
import komod.query.condition.TupleCondition;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * @author keivan
 */
public class ConstantTableExpression implements TableExpression{
    private String tableName;

    public ConstantTableExpression(String tableName) {
        this.tableName = tableName;
    }

    private String fieldNameWithoutTableName(Table table , String fieldName) {
        if (fieldName.startsWith(table.getName()+".")) {
            return fieldName.substring((table.getName().length()+1));
        } else {
            return fieldName;
        }
    }

    /*
    * If there is a equal condition that all of its parents are and lhs of it is a fieldValue
    * in the table and rhs is a constant value , we can use index.
    */
    private TupleCondition findIndexedAndTupleCondition(Table table , TupleCondition tupleCondition) {
        if (tupleCondition == null) {
            return null;
        }
        if (tupleCondition.getClass() == Equal.class) {
            Equal equalCondition = (Equal) tupleCondition;
            if (equalCondition.getLHS() instanceof FieldValue && equalCondition.getRHS() instanceof ConstantValue) {
                FieldValue lhs = (FieldValue) equalCondition.getLHS();
                String fieldName = fieldNameWithoutTableName(table,lhs.getFieldName());
                ColumnIndex index = table.getIndex(fieldName);
                if (index != null) {
                    return tupleCondition;
                }
            }
        }
        if (tupleCondition.getClass() == And.class) {
            And andCondition = (And) tupleCondition;
            TupleCondition tcl = findIndexedAndTupleCondition(table,andCondition.getLHS());
            if (tcl != null)
                return tcl;
            TupleCondition tcr = findIndexedAndTupleCondition(table,andCondition.getRHS());
            if (tcr != null)
                return tcr;
        }
        return null;
    }

    public static List<TableRecord> AddTableNameToFields(TableView tableView , List<TableRecord> tableRecords) {
//        System.err.println("table View name is : " + tableView.getName());
        List<TableRecord> ans = new ArrayList<>();
        for (TableRecord tableRecord : tableRecords) {
            HashMap<String, Object> newMap = new HashMap<>();
            //for (Iterator<HashMap.Entry<String,Object>> it )
            tableRecord.getFieldsMap().forEach((key,value)-> {
                newMap.put(tableView.getName()+"."+key,value); // todo
            });
//            System.err.println(newMap);
            ans.add(new TableRecord(tableView.getBaseTable(),newMap));
        }
        return ans;
    }

    private String removeOneDot(String s) {
        int start = s.lastIndexOf('.')+1;
        if(start < 0 || start >= s.length())
            start = 0;
        return s.substring(start);
    }

    private String removeTableNameAndAddViewName(String s) {
        return tableName+removeOneDot(s);
    }

    public List<TableRecord> getAllRecords(TupleCondition tupleCondition) {
        TableView tableView = Table.getTableOrView(tableName);

        if (tableView instanceof View) {
            View view = (View) tableView;
            SelectFrom constraint = view.getConstraint();
           // TupleCondition newCondition = new And(constraint.getTupleCondition(), tupleCondition);
           /* SelectFrom select = new SelectFrom(
                    constraint.getColumnNames(),
                    constraint.getTableExpression(),
                    constraint.getTupleCondition(),
                    constraint.getGroupByColumns(),
                    constraint.getGroupByTupleCondition());*/

            List<TableRecord> result = constraint.execute();
            // filter columns
            List<TableRecord> filteredResult = new LinkedList<>();


            for (TableRecord record : result) {
                HashMap<String, Object> fieldMap = record.getFieldsMap();
                HashMap<String, Object> filteredMap = new HashMap<>();
                /*for (TableHeader header : view.getHeaders()) {
                    filteredMap.put(removeOneDot(header.getName()),
                            fieldMap.get(header.getName()));
                }*/
                fieldMap.forEach((key, value) -> {
                    String withoutDot = removeOneDot(key);
                    if (view.hasHeader(withoutDot)) {
                        filteredMap.put(withoutDot,value);
                    }
                });


                TableRecord filteredRecord = new TableRecord(view.getBaseTable(), filteredMap);
                filteredRecord.setPre(record.getPre());
                filteredRecord.setNext(record.getNext());
                filteredResult.add(filteredRecord);
            }
            return filteredResult;
        }

        Table table = tableView.getBaseTable();
        TupleCondition indexedEqualTupleCondition = findIndexedAndTupleCondition(table,tupleCondition);
        if (indexedEqualTupleCondition != null && indexedEqualTupleCondition.getClass() == Equal.class) {
            Equal equalCondition = (Equal) indexedEqualTupleCondition;
            if (equalCondition.getLHS() instanceof FieldValue && equalCondition.getRHS() instanceof ConstantValue) {
                FieldValue lhs = (FieldValue) equalCondition.getLHS();
                String fieldName = fieldNameWithoutTableName(table, lhs.getFieldName());
                ColumnIndex index = table.getIndex(fieldName);
                if (index != null) {
                    return new ArrayList<>(index.getRecords(equalCondition.getRHS().evaluate(null))) ;
                }
            }
        }

//            List<TableRecord> myTableRecords = new ArrayList<>();
        LinkedList<TableRecord> myTableRecords = new LinkedList<>();
        for (TableRecord record = table.getLastRecord(); record != null; record = record.getPre()) {
            /*boolean b = tupleCondition.evaluate(record.getFieldsMap());
            if (b) {*/
            myTableRecords.addFirst(record);
            //}
        }
        return myTableRecords;
    }

    @Override
    public List<TableRecord> getPossibleRecords(TupleCondition tupleCondition) {
        return AddTableNameToFields(Table.getTableOrView(tableName),getAllRecords(tupleCondition));
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        tableName = tableName;
    }
}
