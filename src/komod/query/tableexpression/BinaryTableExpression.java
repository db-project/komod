package komod.query.tableexpression;

import java.util.HashMap;

/**
 * Created by Mobin on 12/26/2015 AD.
 */
public abstract class BinaryTableExpression implements TableExpression{
    TableExpression LHS;
    TableExpression RHS;

    protected HashMap<String,Object> unionFields(HashMap<String,Object> l , HashMap<String,Object> r) {
        HashMap<String,Object> ans = new HashMap<>();
        ans.putAll(l);
        ans.putAll(r);
        return ans;
    }

    public TableExpression getLHS() {
        return LHS;
    }

    public void setLHS(TableExpression LHS) {
        this.LHS = LHS;
    }

    public TableExpression getRHS() {
        return RHS;
    }

    public void setRHS(TableExpression RHS) {
        this.RHS = RHS;
    }
}
