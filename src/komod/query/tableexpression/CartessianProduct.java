package komod.query.tableexpression;

import komod.db.Table;
import komod.db.TableRecord;
import komod.query.condition.TupleCondition;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Mobin on 12/26/2015 AD.
 */
public class CartessianProduct extends  BinaryTableExpression {

    @Override
    public List<TableRecord> getPossibleRecords(TupleCondition tupleCondition) {
        if (!getLHS().getClass().equals(ConstantTableExpression.class) ||
                !getRHS().getClass().equals(ConstantTableExpression.class)) {
            //
            return null;
        }

        List<TableRecord> ans = new ArrayList<>();

        List<TableRecord> leftRecords = getLHS().getPossibleRecords(tupleCondition);
        List<TableRecord> rightRecords = getRHS().getPossibleRecords(tupleCondition);
        for (TableRecord leftRecord : leftRecords) {
            for (TableRecord rightRecord : rightRecords) {
                ans.add(new TableRecord
                            (null,
                                unionFields(leftRecord.getFieldsMap(),rightRecord.getFieldsMap())) );
            }
        }
        return ans;
    }
}
