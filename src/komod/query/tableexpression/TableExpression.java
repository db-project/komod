package komod.query.tableexpression;

import komod.db.TableRecord;
import komod.query.condition.TupleCondition;

import java.util.List;

/**
 * Created by Mobin on 12/26/2015 AD.
 */
public interface TableExpression {
    List<TableRecord> getPossibleRecords(TupleCondition tupleCondition);
}
