package komod.query.tableexpression;

import komod.db.*;
import komod.query.condition.TupleCondition;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Mobin on 12/26/2015 AD.
 */
public class Join extends  BinaryTableExpression{

    @Override
    public List<TableRecord> getPossibleRecords(TupleCondition tupleCondition) {
        if (!getLHS().getClass().equals(ConstantTableExpression.class) ||
                !getRHS().getClass().equals(ConstantTableExpression.class)) {
            //
            return null;
        }


        ConstantTableExpression leftExpression = (ConstantTableExpression)getLHS();
        ConstantTableExpression rightExpression = (ConstantTableExpression)getRHS();

        List<TableRecord> ans = new ArrayList<>();

        List<TableRecord> leftRecords = getLHS().getPossibleRecords(tupleCondition);

        Table leftTable = Table.getTable(leftExpression.getTableName());
        TableView rightTable = Table.getTableOrView(rightExpression.getTableName());

        ForeignKey fk = null;

        for (ForeignKey foreignKey : leftTable.getForeignKeys()) {
            if (foreignKey.getReferenceTable().getName().equals(rightTable.getName())) {
                fk = foreignKey;
                break;
            }
        }
        if (fk == null) {
            throw new RuntimeException("foreign key is null");
        }

        // commented System.err.println(leftTable.getName());
        // commented System.err.println(rightTable.getName());
        // commented System.err.println(fk.getColumnName() + " , containing table " + fk.getContainingTable().getName() + " , refrence " + fk.getReferenceTable().getName());

        for (TableRecord leftRecord : leftRecords) {
            /*SelectFrom findQuery = new SelectFrom(
                    pk,null/*rightTable*,leftRecord.getField(fk.getColumnName()) );*/

            ColumnIndex index = rightTable.getIndex(rightTable.getPrimaryKey()/*fk.getColumnName()*/);
            if (index == null) {
                throw new RuntimeException("no index on primary key of right table of join");
            }

            // commented System.err.println("leftRecord = " + leftRecord);
            Object leftRecordFKValue = leftRecord.getField(leftTable.getName()+"."+fk.getColumnName());

            // commented System.err.println("leftRecordFKValue = " + leftRecordFKValue);

            List<TableRecord> rightRecords = index.getRecords(leftRecordFKValue);
            if (rightRecords.size() != 1) {
                throw new RuntimeException("join finds " + rightRecords.size() + " elements instead of one element");
            }
            // commented System.err.println("rightRecords.get(0).getFieldsMap() = " + rightRecords.get(0).getFieldsMap());

            HashMap<String, Object> newMap = new HashMap<>();
            //for (Iterator<HashMap.Entry<String,Object>> it )
            rightRecords.get(0).getFieldsMap().forEach((key,value)-> {
                newMap.put(rightTable.getName()+"."+key,value);
            });

            ans.add(new TableRecord(null,unionFields(
                    leftRecord.getFieldsMap(),
                    newMap)) );
        }
        return ans;
    }
}
