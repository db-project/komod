package komod.query;

import komod.db.ColumnIndex;
import komod.db.Table;

/**
 * Komod, a lightweight DBMS written in Java.
 * Copyright (C) 2015 Komod
 * <p>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
] * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * See LICENCE.txt for more details.
 */
public class CreateIndex implements Query {
    private String indexName;
    private String tableName;
    private String columnName;

    public CreateIndex(String indexName, String tableName, String columnName) {
        // commented System.err.println("create index=" + indexName + " table=" + tableName + " col=" + columnName);
        this.indexName = indexName;
        this.tableName = tableName;
        this.columnName = columnName;
    }

    @Override
    public Object eval() {
        Table myTable = Table.getTable(tableName);
        ColumnIndex myIndex = myTable.getIndex(columnName);
        if (myIndex != null)
            return myIndex;
        // now we are sure that it is null
        myTable.setIndex(columnName, new ColumnIndex(myTable, columnName, indexName));
        return "INDEX CREATED\n";
    }
}
