package komod.query;

import komod.db.DataType;
import komod.db.ForeignKey;
import komod.db.Table;
import komod.db.TableHeader;

/**
 * Komod, a lightweight DBMS written in Java.
 * Copyright (C) 2015 Komod
 * <p>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * See LICENCE.txt for more details.
 */
public class CreateTable implements Query {
    private String tableName;
    private String[] columnNames;
    private String[] dataTypes;
    private String primaryKey;// it could be NULL!!
    private ForeignKey[] foreignKeys;

    public CreateTable(String tableName, String[] columnNames, String[] dataTypes,
                       ForeignKey[] foreignKeys, String primaryKey) {
        // commented System.err.println("create table=" + tableName + " cols=" + Arrays.toString(columnNames) + " types=" + Arrays.toString(dataTypes) + " fks=" + Arrays.toString(foreignKeys) + " pk=" + primaryKey);
        this.tableName = tableName;
        this.columnNames = columnNames;
//        for (int i = 0; i < columnNames.length; i++) {
//            columnNames[i] = normalize(columnNames[i]);
//        }
        this.dataTypes = dataTypes;
        this.foreignKeys = foreignKeys;
        this.primaryKey = primaryKey;
    }

    @Override
    public Object eval() {
        TableHeader[] tableHeaders = new TableHeader[columnNames.length];
        for (int i = 0; i < tableHeaders.length; i++) {
            tableHeaders[i] =  new TableHeader(columnNames[i], DataType.getType(dataTypes[i]));
        }
        Table myTable = new Table(tableName, tableHeaders, primaryKey, foreignKeys);
        for (ForeignKey foreignKey : foreignKeys) {
            foreignKey.getReferenceTable().addLegionerKey(foreignKey);
        }
        Table.insertTableOrView(tableName, myTable);

        if (primaryKey != null) {
            CreateIndex PKIndex = new CreateIndex(primaryKey, tableName, primaryKey);
            PKIndex.eval();
        }
        for (ForeignKey foreignKey : foreignKeys) {
            foreignKey.setContainingTable(tableName);
            CreateIndex FKIndex = new CreateIndex(foreignKey.getColumnName(), tableName,foreignKey.getColumnName());
            FKIndex.eval();
        }

        return "TABLE CREATED\n";
    }
}