package komod.query;

import komod.db.*;

import java.util.Arrays;
import java.util.HashMap;

/**
 * Komod, a lightweight DBMS written in Java.
 * Copyright (C) 2015 Komod
 * <p>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * See LICENCE.txt for more details.
 */
public class InsertInto implements Query {
    private String tableName;
    private String[] values;

    public InsertInto(String tableName, String[] values) {
        // commented System.err.println("insert table=" + tableName + " vals=" + Arrays.toString(values));
        this.tableName = tableName;
        this.values = values;
    }

    @Override
    public Object eval() {
        TableView tableView = Table.getTableOrView(tableName);
        Table baseTable = tableView.getBaseTable();

        if (tableView instanceof View) {
            View view = (View) tableView;
            if (!view.isUpdatable()) {
                throw new RuntimeException("VIEW " + view.getName() + " IS NOT UPDATABLE");
            }
        }

        // pk value
        HashMap<String, Object> fields = new HashMap <>();
        TableHeader[] headers = baseTable.getHeaders();
        for (TableHeader header : headers) {
            String columnName = header.getName();
            try {
                int columnIndex = tableView.getColumnNumber(columnName);
                fields.put(columnName, header.getType().valueOf(values[columnIndex]));
            } catch (Exception ignore) {
                fields.put(columnName, null);
            }
        }
        if (tableView.getPrimaryKey() != null) {
            if (fields.get(tableView.getPrimaryKey()) == null) {
                return "C1 CONSTRAINT FAILED\n";
            }
            if (!baseTable.getIndex(tableView.getPrimaryKey()).getRecords(fields.get(tableView.getPrimaryKey())).isEmpty()) {
                return "C1 CONSTRAINT FAILED\n";
            }
        }
        TableRecord insertedTableRecord = new TableRecord(baseTable, fields);
        if (!checkC2(baseTable, insertedTableRecord)){
            return "C2 CONSTRAINT FAILED\n";
        }

        for (ColumnIndex myIndex : baseTable.getIndexMap().values()){
            myIndex.insert(insertedTableRecord.getField(myIndex.getColumnName()),insertedTableRecord);
        }
        insertedTableRecord.setPre(baseTable.getLastRecord());
        baseTable.addLastRecords(insertedTableRecord);

        return "RECORD INSERTED\n";
    }
    public boolean checkC2(Table myTable, TableRecord row){
        for (ForeignKey fk : myTable.getForeignKeys()){
            if (fk.getReferenceTable().getIndex(fk.getReferenceTable().
                    getPrimaryKey()).getRecords(row.getField(fk.getColumnName())).isEmpty())
                return false;

        }
        return true;
    }

}
