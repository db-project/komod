package komod.query.condition;

import komod.query.computation.Computable;

/**
 * Komod, a lightweight DBMS written in Java.
 * Copyright (C) 2015 Hadi
 * <p>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * See LICENCE.txt for more details.
 */
public abstract class CompareCondition extends TupleCondition {
    public abstract void setLHS(Computable c);
    public abstract void setRHS(Computable c);
    public abstract Computable getLHS();
    public abstract Computable getRHS();
}
