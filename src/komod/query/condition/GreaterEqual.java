package komod.query.condition;

import komod.query.computation.Computable;
import komod.util.Comparators;

import java.util.HashMap;

/**
 * Komod, a lightweight DBMS written in Java.
 * Copyright (C) 2015 Komod
 * <p>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * See LICENCE.txt for more details.
 */
public class GreaterEqual extends CompareCondition {
    private Computable computeValue1, computeValue2;

    public GreaterEqual(Computable computeValue1, Computable computeValue2) {
        this.computeValue1 = computeValue1;
        this.computeValue2 = computeValue2;
    }

    @Override
    public void setLHS(Computable c) {
        this.computeValue1 = c;
    }

    @Override
    public void setRHS(Computable c) {
        this.computeValue2 = c;
    }

    @Override
    public Computable getLHS() {
        return computeValue1;
    }

    @Override
    public Computable getRHS() {
        return computeValue2;
    }

    @Override
    public boolean evaluate(HashMap<String, Object> assignments) {
        Object value1 = computeValue1.evaluate(assignments);
        Object value2 = computeValue2.evaluate(assignments);
        return Comparators.defaultComparator.compare(value1, value2) >= 0;
    }
}
