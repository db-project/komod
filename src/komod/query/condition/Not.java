package komod.query.condition;

import java.util.HashMap;

/**
 * Komod, a lightweight DBMS written in Java.
 * Copyright (C) 2015 Komod
 * <p>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * See LICENCE.txt for more details.
 */
public class Not extends TupleCondition {
    private TupleCondition condition;

    public Not(TupleCondition condition) {
        this.condition = condition;
    }

    public TupleCondition getLHS() { return condition; }
    @Override
    public boolean evaluate(HashMap<String, Object> assignments) {
        return ! condition.evaluate(assignments);
    }
}
