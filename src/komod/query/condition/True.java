package komod.query.condition;

import java.util.HashMap;

/**
 * Komod, a lightweight DBMS written in Java.
 * Copyright (C) 2015 Komod
 * <p>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * See LICENCE.txt for more details.
 */
public class True extends TupleCondition {
    @Override
    public boolean evaluate(HashMap<String, Object> assignments) {
        return true;
    }
}
