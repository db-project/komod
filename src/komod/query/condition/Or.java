package komod.query.condition;

import java.util.HashMap;

/**
 * Komod, a lightweight DBMS written in Java.
 * Copyright (C) 2015 Komod
 * <p>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * See LICENCE.txt for more details.
 */
public class Or extends BinaryCondition {
    private TupleCondition condition1, condition2;

    public Or(TupleCondition condition1, TupleCondition condition2) {
        this.condition1 = condition1;
        this.condition2 = condition2;
    }

    @Override
    public void setLHS(TupleCondition t) {
        this.condition1 = t;
    }

    @Override
    public void setRHS(TupleCondition t) {
        this.condition2 = t;
    }

    public TupleCondition getLHS() {
        return condition1;
    }

    public TupleCondition getRHS() {
        return condition2;
    }

    @Override
    public boolean evaluate(HashMap<String, Object> assignments) {
        return condition1.evaluate(assignments) || condition2.evaluate(assignments);
    }
}
