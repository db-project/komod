package komod.query.condition;

/**
 * Komod, a lightweight DBMS written in Java.
 * Copyright (C) 2015 Komod
 * <p>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * See LICENCE.txt for more details.
 */
public abstract class BinaryCondition extends TupleCondition {
    public abstract void setLHS(TupleCondition t);
    public abstract void setRHS(TupleCondition t);
    public abstract TupleCondition getLHS();
    public abstract TupleCondition getRHS();
}
