package komod.query;

/**
 * Komod, a lightweight DBMS written in Java.
 * Copyright (C) 2015 Komod
 * <p>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * See LICENCE.txt for more details.
 */
public class QuerySet implements Query {
    private Query[] queries;

    public QuerySet(Query[] queries) {
        this.queries = queries;
    }

    @Override
    public Object eval() {
        Object returnValue = null;
        for (Query q : queries) {
            returnValue = q.eval();
        }
        return returnValue;
    }
}
