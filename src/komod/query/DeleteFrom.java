package komod.query;

import komod.db.*;
import komod.query.computation.ConstantValue;
import komod.query.computation.FieldValue;
import komod.query.condition.Equal;
import komod.query.condition.TupleCondition;
import komod.query.tableexpression.ConstantTableExpression;

import java.util.ArrayList;
import java.util.List;

/**
 * Komod, a lightweight DBMS written in Java.
 * Copyright (C) 2015 Komod
 * <p>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * See LICENCE.txt for more details.
 */
public class DeleteFrom implements Query {
    private String tableName;
    private TupleCondition tupleCondition;

    public DeleteFrom(String tableName, TupleCondition tupleCondition) {
        // commented System.err.println("delete table=" + tableName + " tuple=" + tupleCondition);
        this.tableName = tableName;
        this.tupleCondition = tupleCondition;
    }

    @Override
    public Object eval() {
        TableView tableView = Table.getTableOrView(tableName);
        Table baseTable = tableView.getBaseTable();

        if (tableView instanceof View) {
            View view = (View) tableView;
            if (!view.isUpdatable()) {
                throw new RuntimeException("VIEW " + view.getName() + " IS NOT UPDATABLE");
            }
        }

        String[] colNames = new String[tableView.getHeaders().length];
        int cnt=0;
        for(TableHeader header : tableView.getHeaders() ) {
            colNames[cnt] = header.getName();
            cnt++;
        }

        SelectFrom select = new SelectFrom(colNames, new ConstantTableExpression(tableName), tupleCondition);
        List<TableRecord> targetRecords = select.getRecords();

        // now clean the from columnIndexes
        for (TableRecord targetRecord : targetRecords) {
//        for (int i = 0; i < targetRecords.size(); i++) {
            if (!canDeleteRecord(targetRecord))
                return "FOREIGN KEY CONSTRAINT RESTRICTS\n";
            deleteParentRecord(targetRecord);
            for (ColumnIndex index : baseTable.getIndexMap().values()) {
                index.delete(targetRecord.getField(index.getColumnName()), targetRecord);
            }
            targetRecord.remove();
//            targetRecords.remove(targetRecords.get(i));
//            i--;
        }

//        boolean isRestrictAtAll = true;
//
//        ArrayList<Table> pre = new ArrayList<Table>();
//        ArrayList<Table> next = new ArrayList<Table>();
//        pre.add(table);
//        boolean isThereAnySearchForMoreForeignKey = false;
//        boolean isThisTableRestrict = false;
//        while(true) {
//            isThereAnySearchForMoreForeignKey = false;
//            for (Table preTable : pre) {
//
//                for (ForeignKey foreignKey : table.getLegionerKeys()) {
//                    next.add(foreignKey.getContainingTable());
//                    if (foreignKey)
//                    if (foreignKey.getDeleteAction() == "CASCADE"){
//                        isThisTableRestrict = false;
//                    }
//                }
//                if (preTable.getLegionerKeys().size() != 0) {
//                    isThereAnySearchForMoreForeignKey = true;
//                }
//                if (isThisTableRestrict == true) {
//                    isRestrictAtAll = true;
//                    isThereAnySearchForMoreForeignKey = false;
//                }
//            }
//            if (isThereAnySearchForMoreForeignKey == false) {
//                break;
//            }
//        }
        return "";
    }

    public boolean canDeleteRecord(TableRecord record) {
        ArrayList<ForeignKey> fks = record.getTable().getLegionerKeys();
        for (ForeignKey fk : fks) {
            SelectFrom query = new SelectFrom(
                    new String[] {},
                    new ConstantTableExpression(fk.getContainingTable().getName()),
                    new Equal(
                            new FieldValue(fk.getColumnName()),
                            new ConstantValue(record.getField(record.getTable().getPrimaryKey()))
                    )
            );
            List<TableRecord> result = query.getRecords();
            // commented System.err.println(fk.getDeleteAction());
            // commented System.err.println(result);
            if (!result.isEmpty() && fk.getDeleteAction().equals("RESTRICT"))
                return false;
            for (TableRecord foreignRecord : result)
                if (!canDeleteRecord(foreignRecord))
                    return false;
        }
        return true;
    }

    public boolean deleteParentRecord(TableRecord record) {
        ArrayList<ForeignKey> fks = record.getTable().getLegionerKeys();
        for (ForeignKey fk : fks) {
            // commented System.err.println("cname: " + fk.getColumnName());
            SelectFrom query = new SelectFrom(
                    new String[] {},
                    new ConstantTableExpression(fk.getContainingTable().getName()),
                    new Equal(
                            new FieldValue(fk.getColumnName()),
                            new ConstantValue(record.getField(record.getTable().getPrimaryKey()))
                    )
            );
            List<TableRecord> result = query.getRecords();
            // commented System.err.println(result);

            for (TableRecord tr : result){
                deleteParentRecord(tr);
                for (ColumnIndex index : tr.getTable().getIndexMap().values()) {
                    index.delete(tr.getField(index.getColumnName()), tr);
                }
                tr.remove();
            }
        }
        return true;
    }
}
