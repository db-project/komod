package komod.query;

import komod.db.*;
import komod.query.computation.Computable;
import komod.query.computation.ConstantValue;
import komod.query.computation.FieldValue;
import komod.query.condition.Equal;
import komod.query.condition.TupleCondition;
import komod.query.tableexpression.ConstantTableExpression;

import java.util.ArrayList;
import java.util.List;

/**
 * Komod, a lightweight DBMS written in Java.
 * Copyright (C) 2015 Komod
 * <p>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * See LICENCE.txt for more details.
 */
public class UpdateTable implements Query {
    private String tableName;
    private String columnName;
    private Computable computeValue;
    private TupleCondition tupleCondition;

    public UpdateTable(String tableName, String columnName, Computable computeValue, TupleCondition tupleCondition) {
        // commented System.err.println("update table=" + tableName + " column=" + columnName + " comval=" + computeValue + " tuple=" + tupleCondition);
        this.tableName = tableName;
        this.columnName = normalize(columnName);
        this.computeValue = computeValue;
        this.tupleCondition = tupleCondition;
    }

    private String normalize(String columnName) {
        int index = columnName.indexOf('.');
        return index == -1 ? columnName : columnName.substring(index+1);
    }

    @Override
    public Object eval() {
        TableView tableView = Table.getTableOrView(tableName);
        Table baseTable = tableView.getBaseTable();

        if (tableView instanceof View) {
            View view = (View) tableView;
            if (!view.isUpdatable()) {
                throw new RuntimeException("VIEW " + view.getName() + " IS NOT UPDATABLE");
            }
        }

        String[] colNames = new String[tableView.getHeaders().length];
        int cnt = 0;
        for (TableHeader header : tableView.getHeaders())
            colNames[cnt++] = header.getName();

        SelectFrom select = new SelectFrom(colNames, new ConstantTableExpression(tableName), tupleCondition);
        List<TableRecord> updateTargetsRecords = select.getRecords();
        // commented System.err.println(updateTargetsRecords);

        for (TableRecord myRecord : updateTargetsRecords) {
            Object oldValue = myRecord.getField(columnName);
            Object newValue = computeValue.evaluate(myRecord.getFieldsMap());
            if (newValue.equals(oldValue))
                continue;

            if (!isCompatible(newValue))
                return "C1 CONSTRAINT FAILED\n";

            if (!checkC2(baseTable, newValue)){
                return "C2 CONSTRAINT FAILED\n";
            }

            if (!canUpdateRecord(myRecord, newValue, oldValue)) {
                return "FOREIGN KEY CONSTRAINT RESTRICTS\n";
            }
            myRecord.setField(columnName, newValue);
            ColumnIndex index = baseTable.getIndex(columnName);
            if (index != null) {
                index.update(oldValue, newValue, myRecord);
            }
        }
        return "";
    }

    public boolean isCompatible(Object newValue){
        Table myTable = Table.getTableOrView(tableName).getBaseTable();
        if (!columnName.equals(myTable.getPrimaryKey()))
            return true;
        if (newValue == null)
            return false;
//        // commented System.err.println(newValue);
//        // commented System.err.println(myTable.getIndex(columnName).getRecords(newValue).isEmpty());
        if (myTable.getIndex(columnName).getRecords(newValue).isEmpty())
            return true;
        return false;

    }

    public boolean canUpdateRecord(TableRecord record, Object newValue, Object oldValue) {
        Table myTable = Table.getTableOrView(tableName).getBaseTable();
        if (!columnName.equals(myTable.getPrimaryKey()))
            return true;
        ArrayList<ForeignKey> fks = record.getTable().getLegionerKeys();
        for (ForeignKey fk : fks) {
            SelectFrom query = new SelectFrom(
                    new String[] {},
                    new ConstantTableExpression(fk.getContainingTable().getName()),
                    new Equal(
                            new FieldValue(fk.getColumnName()),
                            new ConstantValue(record.getField(record.getTable().getPrimaryKey()))
                    )
            );
            List<TableRecord> result = query.getRecords();

            if (!result.isEmpty() && fk.getUpdateAction().equals("RESTRICT"))
                return false;
            for (TableRecord tr : result){
//                tr.getField(columnName)
                tr.setField(fk.getColumnName(), newValue);
                ColumnIndex index = tr.getTable().getIndex(fk.getColumnName());
                if (index != null) {
                    index.update(oldValue, newValue, tr);
                }
                else{
                    System.out.println("!");
                }
            }
        }
        return true;
    }

    public boolean checkC2(Table myTable, Object newValue){
        for (ForeignKey fk : myTable.getForeignKeys()){
            if (fk.getReferenceTable().getIndex(fk.getReferenceTable().
                    getPrimaryKey()).getRecords(newValue).isEmpty())
                return false;

        }
        return true;
    }
}
