package komod.query;

import komod.db.Table;
import komod.db.View;

/**
 * Created by baroon on 2/2/16.
 */
public class CreateView implements Query{
    private String viewName;
    private SelectFrom selectQuery;

    public CreateView(String viewName, SelectFrom selectQuery) {
        this.viewName = viewName;
        this.selectQuery = selectQuery;
    }

    @Override
    public Object eval() {
        View view = new View(viewName, selectQuery);
        Table.insertTableOrView(viewName, view);
        return "VIEW CREATED\n";
    }
}
