package komod.query;

import komod.db.ForeignKey;
import komod.db.Table;
import komod.db.TableRecord;
import komod.query.computation.Aggregation;
import komod.db.TableView;
import komod.query.computation.BinaryComputation;
import komod.query.computation.Computable;
import komod.query.computation.FieldValue;
import komod.query.condition.BinaryCondition;
import komod.query.condition.CompareCondition;
import komod.query.condition.Not;
import komod.query.condition.TupleCondition;
import komod.query.tableexpression.BinaryTableExpression;
import komod.query.tableexpression.ConstantTableExpression;
import komod.query.tableexpression.Join;
import komod.query.tableexpression.TableExpression;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Komod, a lightweight DBMS written in Java.
 * Copyright (C) 2015 Komod
 * <p>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * See LICENCE.txt for more details.
 */
public class SelectFrom implements Query {
    private String[] columnNames;
    private String[] correctedColumnNames;
    private TupleCondition tupleCondition;
    private TableExpression tableExpression;
    private String[] groupByColumns;
    private TupleCondition groupByTupleCondition;


    public SelectFrom(String[] columnNames, TableExpression tableExpression, TupleCondition tupleCondition, String[] groupByColumns, TupleCondition groupByTupleCondition) {
        //// commented System.err.println("select cols=" + Arrays.toString(columnNames) + " from table=" + tableExpression + " where tuple=" + tupleCondition);
        this.columnNames = columnNames;
        this.tableExpression = tableExpression;
        this.tupleCondition = tupleCondition;
        this.groupByColumns = groupByColumns;
        this.groupByTupleCondition = groupByTupleCondition;
        //for (int i = 0 ; i < columnNames.length ; ++i) {
        // commented System.err.println(columnNames[i]);
        //}
        // commented System.err.println("table expression : " + tableExpression.getClass());
        // commented System.err.println("tuple condition : " + tupleCondition.getClass());

        correctedColumnNames = new String[columnNames.length];
        for (int i = 0; i < columnNames.length; ++i) {
            correctedColumnNames[i] = makeCorrect(columnNames[i]);
        }

        for (int i = 0; i < this.groupByColumns.length; ++i) {
            this.groupByColumns[i] = makeCorrect(this.groupByColumns[i]);
        }

        swapJoin();
    }

    public SelectFrom(String[] columnNames, TableExpression tableExpression, TupleCondition tupleCondition) {
        this(columnNames, tableExpression, tupleCondition, new String[0], null);
    }

//    public SelectFrom(SelectFrom toClone) {
//        this.columnNames = toClone.columnNames.clone();
//        this.correctedColumnNames = toClone.correctedColumnNames.clone();
//        this.tupleCondition = toClone.tupleCondition;
//        this.tableExpression = toClone.tableExpression;
//        this.groupByColumns = toClone.groupByColumns.clone();
//        this.groupByTupleCondition = toClone.groupByTupleCondition;
//    }


    public TupleCondition getTupleCondition() {
        return tupleCondition;
    }

    public TableExpression getTableExpression() {
        return tableExpression;
    }

    public String[] getGroupByColumns() {
        return groupByColumns;
    }

    public String[] getColumnNames() {
        return columnNames;
    }

    public String[] getCorrectedColumnNames() {
        return correctedColumnNames;
    }

    public TupleCondition getGroupByTupleCondition() {
        return groupByTupleCondition;
    }

    private String removeOneDot(String s) {
        int start = s.lastIndexOf('.')+1;
        if(start < 0 || start >= s.length())
            start = 0;
        return s.substring(start);
    }

    @Override
    public Object eval() {
        List<TableRecord> myTableRecords = execute();
        if (myTableRecords.isEmpty())
            return "NO RESULTS\n";
        StringBuilder sb = new StringBuilder("");
        for (int i = 0; i < columnNames.length; i++) {
            if (i != 0)
                sb.append(",");
            sb.append(removeOneDot(columnNames[i]));
        }
        sb.append("\n");
        for (TableRecord record : myTableRecords) {
            for (int i = 0; i < correctedColumnNames.length; ++i) {
                if (i != 0)
                    sb.append(",");
                Object value = record.getField(correctedColumnNames[i]);
                if (value == null) {
                    sb.append("NULL");
                } else {
                    sb.append(value);
                }
            }
            sb.append("\n");
        }
        return sb.toString();
    }

    /* Returns records exactly from the table.
     * Use it when you want UPDATE or DELETE from a table and need reference objects
     * instead of copy objects.
     */
    public List<TableRecord> getRecords() {
//        makeCorrect(tupleCondition);
//        makeCorrect(groupByTupleCondition);
        //// commented System.err.println("inside getRecords()");
        removeAll(tupleCondition);
        if (tableExpression instanceof ConstantTableExpression) {
            List<TableRecord> allTableRecords = ((ConstantTableExpression) tableExpression).getAllRecords(tupleCondition);
            List<TableRecord> answerRecords = new ArrayList<>();
            for (TableRecord tableRecord : allTableRecords) {
                boolean b = tupleCondition.evaluate(tableRecord.getFieldsMap());
                if (b) {
                    answerRecords.add(tableRecord);
                }
            }
            return answerRecords;

        }
        throw new RuntimeException("Only usable for constant table expression.");
    }

    /*
     * Run a query.
     */
    public List<TableRecord> execute() {
        makeCorrect(tupleCondition);
        makeCorrect(groupByTupleCondition);
        List<TableRecord> allTableRecords = tableExpression.getPossibleRecords(tupleCondition);
        List<TableRecord> answerRecords = new ArrayList<>();
        for (TableRecord tableRecord : allTableRecords) {
//            System.err.println("table record --> " + tableRecord);
//            System.err.println("tuple Condition --> " + tupleCondition);
            boolean b = tupleCondition.evaluate(tableRecord.getFieldsMap());
            if (b) {
                answerRecords.add(tableRecord);
            }
        }
        if (groupByColumns.length > 0) {
            return groupBy(answerRecords);
        }
        return answerRecords;
    }

    /*
     * We are given some raw data and we want to process them.
     * First we make a table that is compressed by groupByColumns.
     * Then we return tableRecords that confirm having condition.
     */
    private List<TableRecord> groupBy(List<TableRecord> rowTableRecords) {
        HashMap<ArrayList<Object>, Integer> groupByedTableRecordIndex = new HashMap<>();
        ArrayList<TableRecord> groupByedTableRecords = new ArrayList<>();
        for (TableRecord rowRecord : rowTableRecords) {
            ArrayList<Object> groupByColumnsValues = new ArrayList<>();
            rowRecord.getFieldsMap().forEach((key, value) -> {
                if (isGroupByColumn(key)) {
                    groupByColumnsValues.add(value);
                }
            });
            if (groupByedTableRecordIndex.containsKey(groupByColumnsValues)) {
                addGroupByedValues(
                        groupByedTableRecords.get(groupByedTableRecordIndex.get(groupByColumnsValues))
                        , rowRecord);
            } else {
                groupByedTableRecordIndex.put(groupByColumnsValues, groupByedTableRecords.size());
                groupByedTableRecords.add(makeGroupByedTableRecord(rowRecord));
            }
        }
        List<TableRecord> answer = new ArrayList<>();
        for (TableRecord record : groupByedTableRecords) {
            if (groupByTupleCondition.evaluate(record.getFieldsMap())) {
                answer.add(record);
            }
        }
        return answer;
    }

    private void addGroupByedValues(TableRecord groupByedRecord, TableRecord rowRecord) {
        groupByedRecord.getFieldsMap().forEach((key, value) -> {
            if (value instanceof ArrayList<?>) {
                ArrayList<Object> arrayList = ((ArrayList<Object>) value);
                arrayList.add(rowRecord.getField(key));
            }
        });
    }

    private TableRecord makeGroupByedTableRecord(TableRecord rowRecord) {
        HashMap<String, Object> groupByFields = new HashMap<>();
        rowRecord.getFieldsMap().forEach((key, value) -> {
            if (isGroupByColumn(key)) {
                groupByFields.put(key, value);
            } else {
                groupByFields.put(key, new ArrayList<Object>() {{
                    add(value);
                }});
            }
        });
        return new TableRecord(null, groupByFields);
    }


    private boolean isGroupByColumn(String columnName) {
        for (int i = 0; i < groupByColumns.length; ++i) {
            if (columnName.equals(groupByColumns[i])) {
                return true;
            }
        }
        return false;
    }


    void swapJoin() {
        if (tableExpression instanceof Join) {
            Join join = (Join) tableExpression;
            ForeignKey fk = null;

            ConstantTableExpression leftExpression = (ConstantTableExpression) join.getLHS();
            ConstantTableExpression rightExpression = (ConstantTableExpression) join.getRHS();

            Table leftTable = Table.getTable(leftExpression.getTableName());
            Table rightTable = Table.getTable(rightExpression.getTableName());
            for (ForeignKey foreignKey : leftTable.getForeignKeys()) {
                if (foreignKey.getReferenceTable().getName().equals(rightTable.getName())) {
                    fk = foreignKey;
                    break;
                }
            }
            if (fk == null) {
                Join newJoin = new Join();
                newJoin.setLHS(join.getRHS());
                newJoin.setRHS(join.getLHS());
                tableExpression = newJoin;
            }
        }
    }

    String makeCorrect(String s) {
        // commented System.err.println("make Correct this :" + s);
        if (tableExpression instanceof ConstantTableExpression) {
            ConstantTableExpression cte = (ConstantTableExpression) tableExpression;
//            System.err.println(" in make correct(S) cte.getTableName() = " + cte.getTableName());
            //TableView tableView = Table.getTableOrView(cte.getTableName());
            //Table baseTable = tableView.getBaseTable(); // todo
            if (s.startsWith(cte.getTableName() + ".")) {
                return s;
            } else {
                return cte.getTableName() + "." + s;
            }
        }
        BinaryTableExpression binaryTableExpression = (BinaryTableExpression) tableExpression;
        ConstantTableExpression left = (ConstantTableExpression) binaryTableExpression.getLHS();
        ConstantTableExpression right = (ConstantTableExpression) binaryTableExpression.getRHS();
        // commented System.err.println("//" +left);
        // commented System.err.println(right + " //");
        if (s.startsWith(left.getTableName() + ".") || s.startsWith(right.getTableName() + ".")) {
            return s;
        }
        TableView leftTable = Table.getTableOrView(left.getTableName());
        if (leftTable.getColumnNumber(s) != -1) {
            return leftTable.getName() + "." + s;
        }
        TableView rightTable = Table.getTableOrView(right.getTableName());
        if (rightTable.getColumnNumber(s) != -1) {
            return rightTable.getName() + "." + s;
        }
        throw new RuntimeException("Column is not nor in left or right.");
    }

    void makeCorrect(FieldValue fieldValue) {
        fieldValue.setFieldName(makeCorrect(fieldValue.getFieldName()));
    }

    void makeCorrect(Aggregation aggregation) {
        aggregation.setFieldName(makeCorrect(aggregation.getFieldName()));
    }

    void makeCorrect(Computable computable) {
        if (computable == null) {
            return;
        }
        if (computable instanceof FieldValue) {
            makeCorrect((FieldValue) computable);
        }
        if (computable instanceof Aggregation) {
            makeCorrect((Aggregation) computable);
        }
        if (computable instanceof BinaryComputation) {
            makeCorrect(((BinaryComputation) computable).getLHS());
            makeCorrect(((BinaryComputation) computable).getRHS());
        }
    }

    void makeCorrect(TupleCondition tupleCondition) {
        if (tupleCondition == null) {
            return;
        }
        if (tupleCondition instanceof CompareCondition) {
            CompareCondition compareCondition = (CompareCondition) tupleCondition;
            makeCorrect(compareCondition.getLHS());
            makeCorrect(compareCondition.getRHS());
            /*if (compareCondition.getLHS() instanceof  FieldValue) {
                makeCorrect((FieldValue)compareCondition.getLHS());
            }
            if (compareCondition.getRHS() instanceof  FieldValue) {
                makeCorrect((FieldValue)compareCondition.getRHS());
            }*/
        }
        if (tupleCondition instanceof BinaryCondition) {
            makeCorrect(((BinaryCondition) tupleCondition).getLHS());
            makeCorrect(((BinaryCondition) tupleCondition).getRHS());
        }
        if (tupleCondition instanceof Not) {
            makeCorrect(((Not) tupleCondition).getLHS());
        }
    }

    String removeAll(String s) {
        if (tableExpression instanceof ConstantTableExpression) {
            String start = ((ConstantTableExpression) tableExpression).getTableName() + ".";
            if (s.startsWith(start)) {
                return s.substring(start.length());
            } else {
                return s;
            }
        }
        throw new RuntimeException("removeAll from tuple condition only when constant table expression.");
    }

    void removeAll(FieldValue fieldValue) {
        //// commented System.err.println(fieldValue.getFieldName() + " --> " + removeAll(fieldValue.getFieldName()));

        fieldValue.setFieldName(removeAll(fieldValue.getFieldName()));
    }

    void removeAll(TupleCondition tupleCondition) {
        //// commented System.err.println("remove All");

        if (tupleCondition == null) {
            return;
        }
        //// commented System.err.println(tupleCondition.getClass());
        if (tupleCondition instanceof CompareCondition) {
            CompareCondition compareCondition = (CompareCondition) tupleCondition;
            //// commented System.err.println("binary computation");
            //// commented System.err.println("LHS = " + compareCondition.getLHS().getClass());
            //// commented System.err.println("RHS = " + compareCondition.getRHS().getClass());

            if (compareCondition.getLHS() instanceof FieldValue) {
                removeAll((FieldValue) compareCondition.getLHS());
            }
            if (compareCondition.getRHS() instanceof FieldValue) {
                removeAll((FieldValue) compareCondition.getRHS());
            }
        }
        if (tupleCondition instanceof BinaryCondition) {
            removeAll(((BinaryCondition) tupleCondition).getLHS());
            removeAll(((BinaryCondition) tupleCondition).getRHS());
        }
        if (tupleCondition instanceof Not) {
            removeAll(((Not) tupleCondition).getLHS());
        }
    }

}
