package komod.lang;

/**
 * Komod, a lightweight DBMS written in Java.
 * Copyright (C) 2015 Komod
 * <p>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * See LICENCE.txt for more details.
 */
public enum NonTerminal implements Symbol {
    Q, C, I, U, D, S, TIV, F, Fprime, VALS, VALSprime, IDS, IDSprime, TC, CMPCV, OP, CV, CVprime, DT, // PHASE 1 SYMBOLS
    PK, FK, TEX, TEXP, // PHASE 2 SYMBOLS
    GB, HV; // PHASE 3 SYMBOLS

    @Override
    public String getName() {
        return this.name();
    }
}
