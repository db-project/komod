package komod.lang;

/**
 * Komod, a lightweight DBMS written in Java.
 * Copyright (C) 2015 Komod
 * <p>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * See LICENCE.txt for more details.
 */
public enum Rule {
    R1(NonTerminal.Q, new Symbol[]{NonTerminal.C, Terminal.SEMICOLON}),
    R2(NonTerminal.Q, new Symbol[]{NonTerminal.I, Terminal.SEMICOLON}),
    R3(NonTerminal.Q, new Symbol[]{NonTerminal.U, Terminal.SEMICOLON}),
    R4(NonTerminal.Q, new Symbol[]{NonTerminal.D, Terminal.SEMICOLON}),
    R5(NonTerminal.Q, new Symbol[]{NonTerminal.S, Terminal.SEMICOLON}),
    R6(NonTerminal.C, new Symbol[]{Terminal.CREATE, NonTerminal.TIV}),
    R7(NonTerminal.TIV, new Symbol[]{Terminal.TABLE, Terminal.ID, Terminal.LPARENTH, NonTerminal.F, Terminal.RPARENTH, NonTerminal.PK, NonTerminal.FK}),
    R8(NonTerminal.TIV, new Symbol[]{Terminal.INDEX, Terminal.ID, Terminal.ON, Terminal.ID, Terminal.LPARENTH, Terminal.ID, Terminal.RPARENTH}),
    R9(NonTerminal.F, new Symbol[]{Terminal.ID, NonTerminal.DT, NonTerminal.Fprime}),
    R10(NonTerminal.Fprime, new Symbol[]{Terminal.COMMA, Terminal.ID, NonTerminal.DT, NonTerminal.Fprime}),
    R11(NonTerminal.Fprime, new Symbol[]{}),
    R12(NonTerminal.DT, new Symbol[]{Terminal.INT}),
    R13(NonTerminal.DT, new Symbol[]{Terminal.VARCHAR}),
    R14(NonTerminal.I, new Symbol[]{Terminal.INSERT, Terminal.INTO, Terminal.ID, Terminal.VALUES, Terminal.LPARENTH, NonTerminal.VALS, Terminal.RPARENTH}),
    R15(NonTerminal.VALS, new Symbol[]{Terminal.CONSVAL, NonTerminal.VALSprime}),
    R16(NonTerminal.VALSprime, new Symbol[]{Terminal.COMMA, Terminal.CONSVAL, NonTerminal.VALSprime}),
    R17(NonTerminal.VALSprime, new Symbol[]{}),
    R18(NonTerminal.U, new Symbol[]{Terminal.UPDATE, Terminal.ID, Terminal.SET, Terminal.ID, Terminal.EQ, NonTerminal.CV, Terminal.WHERE, NonTerminal.TC}),
    R19(NonTerminal.D, new Symbol[]{Terminal.DELETE, Terminal.FROM, Terminal.ID, Terminal.WHERE, NonTerminal.TC}),
    R20(NonTerminal.S, new Symbol[]{Terminal.SELECT, NonTerminal.IDS, Terminal.FROM, NonTerminal.TEX, Terminal.WHERE, NonTerminal.TC, NonTerminal.GB}),
    R21(NonTerminal.IDS, new Symbol[]{Terminal.ID, NonTerminal.IDSprime}),
    R22(NonTerminal.IDSprime, new Symbol[]{Terminal.COMMA, Terminal.ID, NonTerminal.IDSprime}),
    R23(NonTerminal.IDSprime, new Symbol[]{}),
    R24(NonTerminal.TC, new Symbol[]{Terminal.TRUE}),
    R25(NonTerminal.TC, new Symbol[]{Terminal.FALSE}),
    R26(NonTerminal.TC, new Symbol[]{NonTerminal.CV, NonTerminal.CMPCV}),
    R27(NonTerminal.TC, new Symbol[]{Terminal.LPARENTH, NonTerminal.TC, Terminal.RPARENTH, NonTerminal.OP, Terminal.LPARENTH, NonTerminal.TC, Terminal.RPARENTH}),
    R28(NonTerminal.TC, new Symbol[]{Terminal.NOT, NonTerminal.TC}),
    R29(NonTerminal.CMPCV, new Symbol[]{Terminal.EQ, NonTerminal.CV}),
    R30(NonTerminal.CMPCV, new Symbol[]{Terminal.GT, NonTerminal.CV}),
    R31(NonTerminal.CMPCV, new Symbol[]{Terminal.LT, NonTerminal.CV}),
    R32(NonTerminal.CMPCV, new Symbol[]{Terminal.GOE, NonTerminal.CV}),
    R33(NonTerminal.CMPCV, new Symbol[]{Terminal.LOE, NonTerminal.CV}),
    R34(NonTerminal.OP, new Symbol[]{Terminal.AND}),
    R35(NonTerminal.OP, new Symbol[]{Terminal.OR}),
    R36(NonTerminal.CV, new Symbol[]{Terminal.CONSVAL, NonTerminal.CVprime}),
    R37(NonTerminal.CV, new Symbol[]{Terminal.ID, NonTerminal.CVprime}),
    R38(NonTerminal.CVprime, new Symbol[]{Terminal.ADD, NonTerminal.CV}),
    R39(NonTerminal.CVprime, new Symbol[]{Terminal.SUB, NonTerminal.CV}),
    R40(NonTerminal.CVprime, new Symbol[]{Terminal.MUL, NonTerminal.CV}),
    R41(NonTerminal.CVprime, new Symbol[]{Terminal.DIV, NonTerminal.CV}),
    R42(NonTerminal.CVprime, new Symbol[]{}),
    // phase 2
    R43(NonTerminal.PK, new Symbol[]{Terminal.PRIMARY, Terminal.KEY, Terminal.ID}),
    R44(NonTerminal.PK, new Symbol[]{}),
    R45(NonTerminal.FK, new Symbol[]{Terminal.FOREIGN, Terminal.KEY, Terminal.ID, Terminal.REFERENCES, Terminal.ID, Terminal.ON, Terminal.DELETE, Terminal.ID, Terminal.ON, Terminal.UPDATE, Terminal.ID, NonTerminal.FK}),
    R46(NonTerminal.FK, new Symbol[]{}),
    R47(NonTerminal.TEX, new Symbol[]{Terminal.ID, NonTerminal.TEXP}),
    R48(NonTerminal.TEXP, new Symbol[]{Terminal.JOIN, Terminal.ID}),
    R49(NonTerminal.TEXP, new Symbol[]{Terminal.COMMA, Terminal.ID}),
    R50(NonTerminal.TEXP, new Symbol[]{}),
    // phase 3
    R51(NonTerminal.GB, new Symbol[]{}),
    R52(NonTerminal.GB, new Symbol[]{Terminal.GROUP, Terminal.BY, NonTerminal.IDS, NonTerminal.HV}),
    R53(NonTerminal.HV, new Symbol[]{}),
    R54(NonTerminal.HV, new Symbol[]{Terminal.HAVING, NonTerminal.TC}),
    R55(NonTerminal.CV, new Symbol[]{Terminal.MAX, Terminal.LPARENTH, Terminal.ID, Terminal.RPARENTH, NonTerminal.CVprime}),
    R56(NonTerminal.CV, new Symbol[]{Terminal.MIN, Terminal.LPARENTH, Terminal.ID, Terminal.RPARENTH, NonTerminal.CVprime}),
    R57(NonTerminal.CV, new Symbol[]{Terminal.SUM, Terminal.LPARENTH, Terminal.ID, Terminal.RPARENTH, NonTerminal.CVprime}),
    R58(NonTerminal.CV, new Symbol[]{Terminal.AVG, Terminal.LPARENTH, Terminal.ID, Terminal.RPARENTH, NonTerminal.CVprime}),
    R59(NonTerminal.TIV, new Symbol[]{Terminal.VIEW, Terminal.ID, Terminal.AS, NonTerminal.S} )
    ;

    private NonTerminal lhs;
    private Symbol[] rhs;

    private Rule(NonTerminal lhs, Symbol[] rhs) {
        this.lhs = lhs;
        this.rhs = rhs;
    }

    public NonTerminal getLeftHandSide() {
        return lhs;
    }

    public Symbol[] getRightHandSide() {
        return rhs;
    }

    @Override
    public String toString() {
        String s = "";
        for (Symbol r : rhs)
            s += r.toString() + " ";
        return String.format("( %s: %s -> %s)", name(), lhs.toString(), s);
    }
}
