package komod.lang;

/**
 * Komod, a lightweight DBMS written in Java.
 * Copyright (C) 2015 Komod
 * <p>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * See LICENCE.txt for more details.
 */
public enum Terminal implements Symbol {
    CREATE, TABLE, INDEX, ID, INSERT, CONSVAL, UPDATE, DELETE, SELECT, INT, VARCHAR, FROM, WHERE, ON, SET, INTO, VALUES, TRUE, FALSE, NOT, AND, OR, // KEYWORDS
    COMMA, LPARENTH, RPARENTH, EQ, GT, LT, GOE, LOE, ADD, SUB, MUL, DIV, SEMICOLON, // PHASE 1 SYMBOLS
    PRIMARY, FOREIGN, KEY, REFERENCES, JOIN, // PHASE 2 SYMBOLS
    GROUP, BY, HAVING, MAX, MIN, SUM, AVG, VIEW, AS // PHASE 3 SYMBOLS
    ;

    @Override
    public String getName() {
        return this.name();
    }
}
