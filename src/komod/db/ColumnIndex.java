package komod.db;

import komod.util.*;

import java.util.*;

/**
 * Komod, a lightweight DBMS written in Java.
 * Copyright (C) 2015 Komod
 * <p>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * See LICENCE.txt for more details.
 */
public class ColumnIndex {
    private TreeMap<Object, ArrayList<TableRecord>> indexMap;
    private String indexName;
    private String columnName;

    public void update(Object oldObj, Object newObj, TableRecord tableRecord) {
        delete(oldObj, tableRecord);
        insert(newObj, tableRecord);
    }

    public ArrayList<TableRecord> getRecords(Object key){
//        System.out.println(key);
        ArrayList<TableRecord> records = indexMap.get(key);
        if (records == null)
            return new ArrayList<>();
        else
            return records;
    }

    public void delete(Object obj, TableRecord correspondingTable) {
        ArrayList<TableRecord> results = indexMap.get(obj);
        if (results == null)
            return;
        if (results.size() > 1) {
            results.remove(correspondingTable);
        } else {
            indexMap.remove(obj);
        }
    }

    public void insert(Object obj, TableRecord correspondingTable) {
        if (indexMap.get(obj) == null) {
            ArrayList<TableRecord> myRecords = new ArrayList<TableRecord>();
            myRecords.add(correspondingTable);
            indexMap.put(obj, myRecords);
        } else {
            indexMap.get(obj).add(correspondingTable);
        }
    }

    public List<TableRecord> Select() {
        List<TableRecord> ans = new ArrayList<TableRecord>();
        return ans;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public ColumnIndex(Table myKomod,String columnName, String indexName) {
        this.indexName = indexName;
        this.columnName = columnName;
        indexMap = new TreeMap<>(Comparators.defaultComparator);
        TableRecord myRecord = myKomod.getLastRecord();
        while (myRecord != null){
            insert(myRecord.getField(columnName),myRecord);
            myRecord = myRecord.getPre();
        }
    }
}

