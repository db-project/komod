package komod.db;

/**
 * Created by Mobin on 12/26/2015 AD.
 */
public class ForeignKey {
    private String deleteAction;
    private String updateAction;
    private String columnName;
    private Table referenceTable;
    private Table containingTable;// is the table that we this foreign key is being used right now

    public ForeignKey(String columnName , String referenceTable,
                      String containingTable , String deleteAction, String updateAction) {
        // commented System.err.println("fk col=" + columnName + " ref=" + referenceTable + " cont=" + containingTable + " del=" + deleteAction + " upd=" + updateAction);
        this.referenceTable = referenceTable != null ? Table.getTable(referenceTable) : null;
        this.containingTable = containingTable != null ? Table.getTable(containingTable) : null;
        this.deleteAction = deleteAction;
        this.updateAction = updateAction;
        this.columnName = columnName;
    }

    public String getDeleteAction() {
        return deleteAction;
    }

    public void setDeleteAction(String deleteAction) {
        this.deleteAction = deleteAction;
    }

    public String getUpdateAction() {
        return updateAction;
    }

    public void setUpdateAction(String updateAction) {
        this.updateAction = updateAction;
    }

    public Table getReferenceTable() {
        return referenceTable;
    }

    public void setReferenceTable(Table referenceTable) {
        this.referenceTable = referenceTable;
    }

    public Table getContainingTable() {
        return containingTable;
    }

    public void setContainingTable(String containingTable) {
        // commented System.err.println("Set containing table: " + containingTable);
        this.containingTable = Table.getTable(containingTable);
    }
    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

}
