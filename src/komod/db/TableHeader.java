package komod.db;

/**
 * Komod, a lightweight DBMS written in Java.
 * Copyright (C) 2015 Komod
 * <p>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * See LICENCE.txt for more details.
 */
public class TableHeader {
    private String name;
    private DataType type;

    public TableHeader(String name, DataType type) {
        this.name = name;
        this.type = type;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public DataType getType() {
        return type;
    }

    @Override
    public String toString() {
        return String.format("Header[%s,%s]", name, type.getClass().getSimpleName());
    }
}
