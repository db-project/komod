package komod.db;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Copyright (C) 2016 Hadi
 */
public interface TableView {
    public String getName();
    public TableHeader[] getHeaders();
    public TableHeader getHeader(String columnName);
    public String getPrimaryKey();
    public Table getBaseTable();
//    public HashMap<String, ColumnIndex> getIndexMap();
    public ColumnIndex getIndex(String columnName);
//    public ForeignKey[] getForeignKeys();
//    public TableRecord getLastRecord();
//    public void setIndex(String columnName, ColumnIndex index);
//    public void addLegionerKey(ForeignKey legionerKey);
//    public ArrayList<ForeignKey> getLegionerKeys();
//    public void setLastRecord(TableRecord record);
    public int getColumnNumber(String str);
//    public void addLastRecords(TableRecord record);
}
