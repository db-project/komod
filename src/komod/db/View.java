package komod.db;

import komod.query.SelectFrom;
import komod.query.tableexpression.ConstantTableExpression;
import komod.query.tableexpression.TableExpression;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Mobin on 2/3/2016 AD.
 */
public class View implements TableView {
    private String name;
    private String primaryKey;
    private SelectFrom constraint;
    private TableHeader[] headers;
    private TableView base;
    private boolean updatable;

    public View(String name, SelectFrom constraint) {
        this.constraint = constraint;
        this.name = name;
        updatable = true;

        // headers
        String[] columns = constraint.getCorrectedColumnNames();
        headers = new TableHeader[columns.length];
        for (int i = 0; i < headers.length; i++) {
            String[] tableColumn = columns[i].split("\\.");
            headers[i] = Table.getTableOrView(tableColumn[0]).getHeader(tableColumn[1]);
        }

        // no join or cross product
        TableExpression tex = constraint.getTableExpression();
        if (tex instanceof ConstantTableExpression) {
            base = Table.getTableOrView(((ConstantTableExpression) tex).getTableName());
        } else {
            updatable = false;
            return;
        }

        // no group by
        if (constraint.getGroupByColumns().length != 0)
            updatable = false;

        // contain primary key
        primaryKey = base.getPrimaryKey();
        //try {
            if (getColumnNumber(primaryKey) == -1) {
                //} catch (Exception ignore) {
                primaryKey = null;
                updatable = false;
            }
        //}

        // no duplicate column
        Set<String> columnSet =  new HashSet<>();
        for (String column : columns) {
            if (!columnSet.add(column)) {
                updatable = false;
                break;
            }
        }
    }

    public SelectFrom getConstraint() {
        return constraint;
    }

    public boolean isUpdatable() {
        return updatable;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public TableHeader[] getHeaders() {
        return headers;
    }

    @Override
    public TableHeader getHeader(String columnName) {
        return headers[getColumnNumber(columnName)];
    }

    @Override
    public String getPrimaryKey() {
        return primaryKey;
    }

    @Override
    public Table getBaseTable() {
        return base == null ? null : base.getBaseTable();
    }

    @Override
    public ColumnIndex getIndex(String columnName) {
        return getBaseTable().getIndex(columnName);
    }

    public boolean hasHeader(String str) {
        for (int i = 0; i < headers.length; i++) {
            if (headers[i].getName().equals(str))
                return true;
        }
        return false;
    }

    @Override
    public int getColumnNumber(String str) {
        for (int i = 0; i < headers.length; i++) {
            if (headers[i].getName().equals(str))
                return i;
        }
        return -1;
//        throw new RuntimeException("Column '" + str + "' does not exists.");
    }
}
