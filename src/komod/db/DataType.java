package komod.db;

/**
 * Komod, a lightweight DBMS written in Java.
 * Copyright (C) 2015 Komod
 * <p>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * See LICENCE.txt for more details.
 */
public abstract class DataType {
    public static final DataType INTEGER = new IntegerDataType();
    public static final DataType VARCHAR = new VarcharDataType();

    public static DataType getType(String name) {
        switch (name) {
            case "INTEGER":
                return INTEGER;
            case "VARCHAR":
                return VARCHAR;
            default:
                throw new RuntimeException("DataType '" + name + "' does not exists.");
        }
    }

    public abstract Object valueOf(String stringValue);

    public abstract String getName();
}

class IntegerDataType extends DataType {
    @Override
    public Object valueOf(String stringValue) {
        if (stringValue == null)
            return null;
        return Integer.parseInt(stringValue);
    }

    @Override
    public String getName() {
        return "INTEGER";
    }
}

class VarcharDataType extends DataType {
    @Override
    public Object valueOf(String stringValue) {
        return stringValue;
    }

    @Override
    public String getName() {
        return "VARCHAR";
    }
}