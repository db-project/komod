package komod.db;

import java.util.HashMap;

/**
 * Komod, a lightweight DBMS written in Java.
 * Copyright (C) 2015 Komod
 * <p>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * See LICENCE.txt for more details.
 */
public class TableRecord {
    private HashMap<String, Object> fields;
    private TableRecord pre;
    private TableRecord next;
    private Table table;

    public TableRecord(Table table, HashMap<String, Object> incomeFields) {
        this.table = table;
        next = null;
        pre = null;
        fields = incomeFields;
    }

    public Object getField(String columnName) {
        if (fields.containsKey(columnName))
            return fields.get(columnName);
        throw new RuntimeException("Column '" + columnName + "' does not exists.");
    }

    public void setField(String columnName, Object value) {
        fields.put(columnName, value);
    }

    public HashMap<String, Object> getFieldsMap() {
        return fields;
    }

    public void setFields(HashMap<String, Object> fields) {
        this.fields = fields;
    }

    public TableRecord getNext() {
        return next;
    }

    public void setNext(TableRecord next) {
        this.next = next;
    }

    public TableRecord getPre() {
        return pre;
    }

    public void setPre(TableRecord pre) {
        this.pre = pre;
    }

    public Table getTable() {
        return table;
    }

    public void remove() {
        if (next == null){
            table.setLastRecord(pre);
        }
        if (next != null) {
            next.pre = pre;
        }
        if (pre != null) {
            pre.next = next;
        }

    }

    @Override
    public String toString() {
        return fields.toString();
    }
}
