package komod.db;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Komod, a lightweight DBMS written in Java.
 * Copyright (C) 2015 Komod
 * <p>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * See LICENCE.txt for more details.
 */
public class Table implements TableView {
    private static Map<String, TableView> tables = new HashMap<>();

    public static void insertTableOrView(String tableViewName, TableView tableView) {
        if (tables.containsKey(tableViewName))
            throw new RuntimeException("Table or view '" + tableViewName + "' already exists.");
        tables.put(tableViewName, tableView);
    }

    public static TableView getTableOrView(String tableViewName) {
        TableView t = tables.get(tableViewName);
        if (t == null)
            throw new RuntimeException("Table or view '" + tableViewName + "' does not exists.");
        return t;
    }

    public static Table getTable(String tableName) {
        TableView tableView = getTableOrView(tableName);
        if (tableView instanceof Table)
            return (Table) tableView;
        throw new RuntimeException("Table '" + tableName + "' does not exists.");
    }

    public static View getView(String viewName) {
        TableView tableView = getTableOrView(viewName);
        if (tableView instanceof View)
            return (View) tableView;
        throw new RuntimeException("View '" + viewName + "' does not exists.");
    }

    private String name;
    private TableHeader[] headers;
    private String primaryKey;// it could be NULL!!
    private ForeignKey[] foreignKeys;
    private ArrayList<ForeignKey> legionerKeys = new ArrayList<>();
    private HashMap<String, ColumnIndex> indices = new HashMap<>();
    private TableRecord lastRecord;

    public Table(String name, TableHeader[] headers, String primaryKey, ForeignKey[] foreignKeys) {
        this.name = name;
        this.headers = headers;
        this.primaryKey = primaryKey;
        this.foreignKeys = foreignKeys;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public TableHeader[] getHeaders() {
        return headers;
    }

    @Override
    public TableHeader getHeader(String columnName) {
        return headers[getColumnNumber(columnName)];
    }

    @Override
    public String getPrimaryKey() {
        return primaryKey;
    }

    @Override
    public Table getBaseTable() {
        return this;
    }

    public HashMap<String, ColumnIndex> getIndexMap() {
        return indices;
    }

    public ColumnIndex getIndex(String columnName) {
        return indices.get(columnName);
    }

    public ForeignKey[] getForeignKeys() {
        return foreignKeys;
    }

    public TableRecord getLastRecord() {
        return lastRecord;
    }

    public void setLastRecord(TableRecord record) {
        lastRecord = record;
    }

    public void setIndex(String columnName, ColumnIndex index) {
        this.indices.put(columnName, index);
    }

    public void addLegionerKey(ForeignKey legionerKey) {
        legionerKeys.add(legionerKey);
    }

    public ArrayList<ForeignKey> getLegionerKeys() {
        return legionerKeys;
    }

    public int getColumnNumber(String str) {
        for (int i = 0; i < headers.length; i++) {
            if (headers[i].getName().equals(str))
                return i;
        }
        //throw new RuntimeException("Column '" + str + "' does not exists.");
        return -1;
    }

    public void addLastRecords(TableRecord record) {
        if (lastRecord != null) {
            lastRecord.setNext(record);
            record.setPre(lastRecord);
        }
        lastRecord = record;
    }

}
