package komod.interpretation;

import komod.interpretation.parser.ParseNode;
import komod.interpretation.parser.Parser;
import komod.interpretation.parser.SemanticRules;
import komod.interpretation.scanner.EOFReachException;
import komod.query.Query;

import java.io.IOException;

/**
 * Komod, a lightweight DBMS written in Java.
 * Copyright (C) 2015 Komod
 * <p>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * See LICENCE.txt for more details.
 */
public class KomodCMD {
    public static void main(String... args) throws IOException {
        Parser p = new Parser();
        ParseNode parseNode;
        do {
            try {
                parseNode = p.parse();
            } catch (EOFReachException ignored) {
                return;
            }
//            System.err.println(parseNode);
//            if (true)
//                continue;
//            System.out.println(parseNode);
//            if (true)
//                continue;
            Object q = SemanticRules.applySemanticRules(parseNode);
//            System.out.println(q);
            try {
                Object result = ((Query) q).eval();
                System.out.print(result);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        } while (parseNode != null);
    }
}
