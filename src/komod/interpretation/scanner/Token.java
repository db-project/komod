package komod.interpretation.scanner;

import komod.lang.Symbol;
import komod.lang.Terminal;

/**
 * Komod, a lightweight DBMS written in Java.
 * Copyright (C) 2015 Komod
 * <p>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * See LICENCE.txt for more details.
 */
public class Token implements Symbol {
    private String value;
    private Terminal terminal;

    public Token(Terminal terminal, String value) {
        this.terminal = terminal;
        this.value = value;
    }

    public Terminal getTerminal() {
        return terminal;
    }

    @Override
    public String getName() {
        return terminal.getName();
    }

    public String getValue() {
        return value;
    }

    public boolean match(String s) {
        return value.equals(s);
    }

    @Override
    public String toString() {
        return String.format("<Token terminal=%s value=%s>", terminal.name(), value);
    }
}
