package komod.interpretation.scanner;

import komod.lang.Terminal;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Komod, a lightweight DBMS written in Java.
 * Copyright (C) 2015 Komod
 * <p>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * See LICENCE.txt for more details.
 */

/**
 * Scanner reads input and returns next token.
 */
public class Scanner {
    private final InputStreamReader isr;
    private char buffers[][], tokenBuffer[];
    private int bufferIndex, cursor, tokenLength;

    // performance stuff
    private int[] charType;
    private Map<String, Terminal> terminalMap;
    private Set<String> keywords;

    public Scanner(InputStream is, int bufferLength, int maxTokenLength) {
        this.isr = new InputStreamReader(is);

        // init buffers
        this.buffers = new char[2][bufferLength];
        this.tokenBuffer = new char[maxTokenLength];

        // init state
        this.cursor = bufferLength;
        this.bufferIndex = 1;

        // performance stuff
        charType = new int[(int) (char) (-1) + 1];
        char[] whiteSpaces = {' ', '\n', '\t', '\r'};
        char[] separators = {',', ';', '(', ')', '=', '+', '-', '*', '/'};
        for (char c : whiteSpaces)
            charType[c] = -1;
        for (char c : separators)
            charType[c] = 1;
        charType['<'] = 2;
        charType['>'] = 2;
        charType['"'] = 3;
        for (int i = 0; i < 10; i++)
            charType['0' + i] = 4;

        terminalMap = new HashMap<>();
        terminalMap.put(",", Terminal.COMMA);
        terminalMap.put(";", Terminal.SEMICOLON);
        terminalMap.put("(", Terminal.LPARENTH);
        terminalMap.put(")", Terminal.RPARENTH);
        terminalMap.put("=", Terminal.EQ);
        terminalMap.put("+", Terminal.ADD);
        terminalMap.put("-", Terminal.SUB);
        terminalMap.put("*", Terminal.MUL);
        terminalMap.put("/", Terminal.DIV);
        terminalMap.put(">", Terminal.GT);
        terminalMap.put("<", Terminal.LT);

        keywords = new HashSet<>();
        keywords.add("CREATE");
        keywords.add("TABLE");
        keywords.add("INDEX");
        keywords.add("INSERT");
        keywords.add("UPDATE");
        keywords.add("DELETE");
        keywords.add("SELECT");
        keywords.add("INT");
        keywords.add("VARCHAR");
        keywords.add("FROM");
        keywords.add("WHERE");
        keywords.add("ON");
        keywords.add("SET");
        keywords.add("INTO");
        keywords.add("VALUES");
        keywords.add("TRUE");
        keywords.add("FALSE");
        keywords.add("NOT");
        keywords.add("AND");
        keywords.add("OR");
        keywords.add("PRIMARY");
        keywords.add("FOREIGN");
        keywords.add("KEY");
        keywords.add("REFERENCES");
        keywords.add("JOIN");
        keywords.add("GROUP");
        keywords.add("BY");
        keywords.add("HAVING");
        keywords.add("MAX");
        keywords.add("MIN");
        keywords.add("SUM");
        keywords.add("AVG");
        keywords.add("VIEW");
        keywords.add("AS");
    }

    public Scanner() {
        this(System.in, 1024, 2048);
    }

    private char getNextChar() throws IOException {
        char c;
        cursor++;
        try {
            c = buffers[bufferIndex][cursor];
        } catch (IndexOutOfBoundsException ignore) {
            c = 0;
        }
        if (c == 0) {
            // load t' other buffer
            char nextBuffer[] = buffers[1 - bufferIndex];
            int count = isr.read(nextBuffer);
            if (count == -1)
                throw new EOFReachException();
            if (count < nextBuffer.length)
                nextBuffer[count] = 0;

            // change state
            bufferIndex = 1 - bufferIndex;
            cursor = 0;

            c = buffers[bufferIndex][cursor];
        }
        return c;
    }

    public Token getNextToken() throws IOException {
        tokenLength = 0;
        char current = ' ';
        while (charType[current] == -1)
            current = getNextChar();
        if (charType[current] != 0) {
            if (charType[current] == 1) {
                // single letter separator
                return new Token(terminalMap.get("" + current), null);
            }
            if (charType[current] == 2) {
                // <= or >=
                char next = getNextChar();
                if (next == '=') {
                    return new Token(current == '<' ? Terminal.LOE : Terminal.GOE, null);
                } else {
                    cursor--;
                    return new Token(current == '<' ? Terminal.LT : Terminal.GT, null);
                }
            }
            if (charType[current] == 3) {
                // constant string
                char next;
                while ((next = getNextChar()) != '"')
                    tokenBuffer[tokenLength++] = next;
                return new Token(Terminal.CONSVAL, new String(tokenBuffer, 0, tokenLength));
            }
            if (charType[current] == 4) {
                // constant number
                cursor--;
                char next;
                while (charType[next = getNextChar()] == 4)
                    tokenBuffer[tokenLength++] = next;
                cursor--;
                return new Token(Terminal.CONSVAL, new String(tokenBuffer, 0, tokenLength));
            }
        } else {
            cursor--;
        }

        // id or keyword
        while ((charType[current = getNextChar()] & 3) == 0)
            tokenBuffer[tokenLength++] = current;
        cursor--;
        String value = new String(tokenBuffer, 0, tokenLength);
        String valueUppercase = value.toUpperCase();
        if (keywords.contains(valueUppercase))
            return new Token(Terminal.valueOf(valueUppercase), null);
        if (valueUppercase.equals("NULL"))
            return new Token(Terminal.CONSVAL, null);
        return new Token(Terminal.ID, new String(tokenBuffer, 0, tokenLength));
    }

    public static void main(String[] args) throws IOException {
        System.out.println("You are testing Komod's scanner.");
        System.out.println("Enter a query and check corresponding tokens to ensure that scanner works correctly.");
        Scanner s = new Scanner();
        while (true) {
            System.out.println(s.getNextToken());
        }
    }

}
