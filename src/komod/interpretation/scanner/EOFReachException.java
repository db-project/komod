package komod.interpretation.scanner;

/**
 * Komod, a lightweight DBMS written in Java.
 * Copyright (C) 2015 Komod
 * <p>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * See LICENCE.txt for more details.
 */
public class EOFReachException extends RuntimeException {

    public EOFReachException() {
        super();
    }

    public EOFReachException(String message) {
        super(message);
    }

    public EOFReachException(String message, Throwable cause) {
        super(message, cause);
    }

    public EOFReachException(Throwable cause) {
        super(cause);
    }

    protected EOFReachException(String message, Throwable cause,
                                boolean enableSuppression,
                                boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
