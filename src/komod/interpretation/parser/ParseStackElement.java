package komod.interpretation.parser;

import komod.lang.Symbol;

/**
 * Komod, a lightweight DBMS written in Java.
 * Copyright (C) 2015 Komod
 * <p>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * See LICENCE.txt for more details.
 */
public class ParseStackElement {
    private Symbol symbol;
    private ParseNode node;

    public ParseStackElement(Symbol symbol, ParseNode node) {
        this.symbol = symbol;
        this.node = node;
    }

    public Symbol getSymbol() {
        return this.symbol;
    }

    public ParseNode getParseNode() {
        return this.node;
    }

    @Override
    public String toString() {
        return String.format("%s", symbol);
    }
}
