package komod.interpretation.parser;

import komod.interpretation.scanner.Scanner;
import komod.interpretation.scanner.Token;
import komod.lang.*;

import java.io.IOException;
import java.util.Stack;

/**
 * Komod, a lightweight DBMS written in Java.
 * Copyright (C) 2015 Komod
 * <p>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * See LICENCE.txt for more details.
 */
public class Parser {
    private Scanner scanner;

    public Parser(Scanner scanner) {
        this.scanner = scanner;
    }

    public Parser() {
        this(new Scanner());
    }

    public ParseNode parse() throws IOException {
        Stack<ParseStackElement> parseStack = new Stack<>();
        ParseNode root = new ParseNode();
        parseStack.push(new ParseStackElement(NonTerminal.Q, root));
        Token token = null;
        do {
//            System.out.println(parseStack);
//            System.out.println(token);
            if (token == null)
                token = scanner.getNextToken();
            ParseStackElement x = parseStack.peek();
            Symbol xSymbol = x.getSymbol();
            ParseNode xNode = x.getParseNode();
//            System.out.println(x);
//            System.out.println(token);
            if (xSymbol instanceof Terminal) {
                if (xSymbol.equals(token.getTerminal())) {
                    xNode.setValue(token.getValue());
                    parseStack.pop();
                    token = null;
                } else {
                    // commented System.err.println("Parse stack: " + parseStack);
                    // commented System.err.println("Current token: " + token);
                    throw new ParseException("Syntax error near " + token.getValue());
                }
            } else {
                Rule r = ParseTable.at((NonTerminal) xSymbol, token.getTerminal());
//                // commented System.err.println(token.getTerminal());
//                // commented System.err.println(token.getTerminal().ordinal());
//                System.out.println(r);
                if (r != null) {
                    parseStack.pop();
                    xNode.setRule(r);
                    Symbol[] rhs = r.getRightHandSide();
                    ParseNode[] children = new ParseNode[rhs.length];
                    for (int i = 0; i < children.length; i++)
                        xNode.addChild(children[i] = new ParseNode());
                    for (int i = 0; i < rhs.length; i++)
                        parseStack.push(new ParseStackElement(rhs[rhs.length-i-1], children[rhs.length-i-1]));
                } else {
//                    System.err.println("Parse stack: " + parseStack);
//                    System.err.println("Current token: " + token);
                    throw new ParseException("Syntax error near " + token.getValue());
                }
            }
        } while (!parseStack.empty());
        return root;
    }
}
