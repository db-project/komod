package komod.interpretation.parser;

import komod.lang.NonTerminal;
import komod.lang.Rule;
import komod.lang.Terminal;

/**
 * Komod, a lightweight DBMS written in Java.
 * Copyright (C) 2015 Komod
 * <p>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * See LICENCE.txt for more details.
 */
public class ParseTable {
    private static Rule[][] table = {
			//Q
			{/*CREATE*/ Rule.R1, /*TABLE*/ null, /*INDEX*/ null, /*ID*/ null, /*INSERT*/ Rule.R2, /*CONSVAL*/ null, /*UPDATE*/ Rule.R3, /*DELETE*/ Rule.R4, /*SELECT*/ Rule.R5, /*INT*/ null, /*VARCHAR*/ null, /*FROM*/ null, /*WHERE*/ null, /*ON*/ null, /*SET*/ null, /*INTO*/ null, /*VALUES*/ null, /*TRUE*/ null, /*FALSE*/ null, /*NOT*/ null, /*AND*/ null, /*OR*/ null, /*COMMA*/ null, /*LPARENTH*/ null, /*RPARENTH*/ null, /*EQ*/ null, /*GT*/ null, /*LT*/ null, /*GOE*/ null, /*LOE*/ null, /*ADD*/ null, /*SUB*/ null, /*MUL*/ null, /*DIV*/ null, /*SEMICOLON*/ null,
					/*PRIMARY*/ null, /*FOREIGN*/ null, /*KEY*/ null, /*REFERENCES*/ null, /*JOIN*/ null,
					/*GROUP*/ null, /*BY*/ null, /*HAVING*/ null, /*MAX*/ null, /*MIN*/ null, /*SUM*/ null, /*AVG*/ null, /*VIEW*/ null, /*AS*/ null},
			//C
			{/*CREATE*/ Rule.R6, /*TABLE*/ null, /*INDEX*/ null, /*ID*/ null, /*INSERT*/ null, /*CONSVAL*/ null, /*UPDATE*/ null, /*DELETE*/ null, /*SELECT*/ null, /*INT*/ null, /*VARCHAR*/ null, /*FROM*/ null, /*WHERE*/ null, /*ON*/ null, /*SET*/ null, /*INTO*/ null, /*VALUES*/ null, /*TRUE*/ null, /*FALSE*/ null, /*NOT*/ null, /*AND*/ null, /*OR*/ null, /*COMMA*/ null, /*LPARENTH*/ null, /*RPARENTH*/ null, /*EQ*/ null, /*GT*/ null, /*LT*/ null, /*GOE*/ null, /*LOE*/ null, /*ADD*/ null, /*SUB*/ null, /*MUL*/ null, /*DIV*/ null, /*SEMICOLON*/ null,
					/*PRIMARY*/ null, /*FOREIGN*/ null, /*KEY*/ null, /*REFERENCES*/ null, /*JOIN*/ null,
					/*GROUP*/ null, /*BY*/ null, /*HAVING*/ null, /*MAX*/ null, /*MIN*/ null, /*SUM*/ null, /*AVG*/ null, /*VIEW*/ null, /*AS*/ null},
			//I
			{/*CREATE*/ null, /*TABLE*/ null, /*INDEX*/ null, /*ID*/ null, /*INSERT*/ Rule.R14, /*CONSVAL*/ null, /*UPDATE*/ null, /*DELETE*/ null, /*SELECT*/ null, /*INT*/ null, /*VARCHAR*/ null, /*FROM*/ null, /*WHERE*/ null, /*ON*/ null, /*SET*/ null, /*INTO*/ null, /*VALUES*/ null, /*TRUE*/ null, /*FALSE*/ null, /*NOT*/ null, /*AND*/ null, /*OR*/ null, /*COMMA*/ null, /*LPARENTH*/ null, /*RPARENTH*/ null, /*EQ*/ null, /*GT*/ null, /*LT*/ null, /*GOE*/ null, /*LOE*/ null, /*ADD*/ null, /*SUB*/ null, /*MUL*/ null, /*DIV*/ null, /*SEMICOLON*/ null,
					/*PRIMARY*/ null, /*FOREIGN*/ null, /*KEY*/ null, /*REFERENCES*/ null, /*JOIN*/ null,
					/*GROUP*/ null, /*BY*/ null, /*HAVING*/ null, /*MAX*/ null, /*MIN*/ null, /*SUM*/ null, /*AVG*/ null, /*VIEW*/ null, /*AS*/ null},
			//U
			{/*CREATE*/ null, /*TABLE*/ null, /*INDEX*/ null, /*ID*/ null, /*INSERT*/ null, /*CONSVAL*/ null, /*UPDATE*/ Rule.R18, /*DELETE*/ null, /*SELECT*/ null, /*INT*/ null, /*VARCHAR*/ null, /*FROM*/ null, /*WHERE*/ null, /*ON*/ null, /*SET*/ null, /*INTO*/ null, /*VALUES*/ null, /*TRUE*/ null, /*FALSE*/ null, /*NOT*/ null, /*AND*/ null, /*OR*/ null, /*COMMA*/ null, /*LPARENTH*/ null, /*RPARENTH*/ null, /*EQ*/ null, /*GT*/ null, /*LT*/ null, /*GOE*/ null, /*LOE*/ null, /*ADD*/ null, /*SUB*/ null, /*MUL*/ null, /*DIV*/ null, /*SEMICOLON*/ null,
					/*PRIMARY*/ null, /*FOREIGN*/ null, /*KEY*/ null, /*REFERENCES*/ null, /*JOIN*/ null,
					/*GROUP*/ null, /*BY*/ null, /*HAVING*/ null, /*MAX*/ null, /*MIN*/ null, /*SUM*/ null, /*AVG*/ null, /*VIEW*/ null, /*AS*/ null},
			//D
			{/*CREATE*/ null, /*TABLE*/ null, /*INDEX*/ null, /*ID*/ null, /*INSERT*/ null, /*CONSVAL*/ null, /*UPDATE*/ null, /*DELETE*/ Rule.R19, /*SELECT*/ null, /*INT*/ null, /*VARCHAR*/ null, /*FROM*/ null, /*WHERE*/ null, /*ON*/ null, /*SET*/ null, /*INTO*/ null, /*VALUES*/ null, /*TRUE*/ null, /*FALSE*/ null, /*NOT*/ null, /*AND*/ null, /*OR*/ null, /*COMMA*/ null, /*LPARENTH*/ null, /*RPARENTH*/ null, /*EQ*/ null, /*GT*/ null, /*LT*/ null, /*GOE*/ null, /*LOE*/ null, /*ADD*/ null, /*SUB*/ null, /*MUL*/ null, /*DIV*/ null, /*SEMICOLON*/ null,
					/*PRIMARY*/ null, /*FOREIGN*/ null, /*KEY*/ null, /*REFERENCES*/ null, /*JOIN*/ null,
					/*GROUP*/ null, /*BY*/ null, /*HAVING*/ null, /*MAX*/ null, /*MIN*/ null, /*SUM*/ null, /*AVG*/ null, /*VIEW*/ null, /*AS*/ null},
			//S
			{/*CREATE*/ null, /*TABLE*/ null, /*INDEX*/ null, /*ID*/ null, /*INSERT*/ null, /*CONSVAL*/ null, /*UPDATE*/ null, /*DELETE*/ null, /*SELECT*/ Rule.R20, /*INT*/ null, /*VARCHAR*/ null, /*FROM*/ null, /*WHERE*/ null, /*ON*/ null, /*SET*/ null, /*INTO*/ null, /*VALUES*/ null, /*TRUE*/ null, /*FALSE*/ null, /*NOT*/ null, /*AND*/ null, /*OR*/ null, /*COMMA*/ null, /*LPARENTH*/ null, /*RPARENTH*/ null, /*EQ*/ null, /*GT*/ null, /*LT*/ null, /*GOE*/ null, /*LOE*/ null, /*ADD*/ null, /*SUB*/ null, /*MUL*/ null, /*DIV*/ null, /*SEMICOLON*/ null,
					/*PRIMARY*/ null, /*FOREIGN*/ null, /*KEY*/ null, /*REFERENCES*/ null, /*JOIN*/ null,
					/*GROUP*/ null, /*BY*/ null, /*HAVING*/ null, /*MAX*/ null, /*MIN*/ null, /*SUM*/ null, /*AVG*/ null, /*VIEW*/ null, /*AS*/ null},
			//TIV
			{/*CREATE*/ null, /*TABLE*/ Rule.R7, /*INDEX*/ Rule.R8, /*ID*/ null, /*INSERT*/ null, /*CONSVAL*/ null, /*UPDATE*/ null, /*DELETE*/ null, /*SELECT*/ null, /*INT*/ null, /*VARCHAR*/ null, /*FROM*/ null, /*WHERE*/ null, /*ON*/ null, /*SET*/ null, /*INTO*/ null, /*VALUES*/ null, /*TRUE*/ null, /*FALSE*/ null, /*NOT*/ null, /*AND*/ null, /*OR*/ null, /*COMMA*/ null, /*LPARENTH*/ null, /*RPARENTH*/ null, /*EQ*/ null, /*GT*/ null, /*LT*/ null, /*GOE*/ null, /*LOE*/ null, /*ADD*/ null, /*SUB*/ null, /*MUL*/ null, /*DIV*/ null, /*SEMICOLON*/ null,
					/*PRIMARY*/ null, /*FOREIGN*/ null, /*KEY*/ null, /*REFERENCES*/ null, /*JOIN*/ null,
					/*GROUP*/ null, /*BY*/ null, /*HAVING*/ null, /*MAX*/ null, /*MIN*/ null, /*SUM*/ null, /*AVG*/ null, /*VIEW*/ Rule.R59, /*AS*/ null},
			//F
			{/*CREATE*/ null, /*TABLE*/ null, /*INDEX*/ null, /*ID*/ Rule.R9, /*INSERT*/ null, /*CONSVAL*/ null, /*UPDATE*/ null, /*DELETE*/ null, /*SELECT*/ null, /*INT*/ null, /*VARCHAR*/ null, /*FROM*/ null, /*WHERE*/ null, /*ON*/ null, /*SET*/ null, /*INTO*/ null, /*VALUES*/ null, /*TRUE*/ null, /*FALSE*/ null, /*NOT*/ null, /*AND*/ null, /*OR*/ null, /*COMMA*/ null, /*LPARENTH*/ null, /*RPARENTH*/ null, /*EQ*/ null, /*GT*/ null, /*LT*/ null, /*GOE*/ null, /*LOE*/ null, /*ADD*/ null, /*SUB*/ null, /*MUL*/ null, /*DIV*/ null, /*SEMICOLON*/ null,
					/*PRIMARY*/ null, /*FOREIGN*/ null, /*KEY*/ null, /*REFERENCES*/ null, /*JOIN*/ null,
					/*GROUP*/ null, /*BY*/ null, /*HAVING*/ null, /*MAX*/ null, /*MIN*/ null, /*SUM*/ null, /*AVG*/ null, /*VIEW*/ null, /*AS*/ null},
			//Fprime
			{/*CREATE*/ null, /*TABLE*/ null, /*INDEX*/ null, /*ID*/ null, /*INSERT*/ null, /*CONSVAL*/ null, /*UPDATE*/ null, /*DELETE*/ null, /*SELECT*/ null, /*INT*/ null, /*VARCHAR*/ null, /*FROM*/ null, /*WHERE*/ null, /*ON*/ null, /*SET*/ null, /*INTO*/ null, /*VALUES*/ null, /*TRUE*/ null, /*FALSE*/ null, /*NOT*/ null, /*AND*/ null, /*OR*/ null, /*COMMA*/ Rule.R10, /*LPARENTH*/ null, /*RPARENTH*/ Rule.R11, /*EQ*/ null, /*GT*/ null, /*LT*/ null, /*GOE*/ null, /*LOE*/ null, /*ADD*/ null, /*SUB*/ null, /*MUL*/ null, /*DIV*/ null, /*SEMICOLON*/ null,
					/*PRIMARY*/ null, /*FOREIGN*/ null, /*KEY*/ null, /*REFERENCES*/ null, /*JOIN*/ null,
					/*GROUP*/ null, /*BY*/ null, /*HAVING*/ null, /*MAX*/ null, /*MIN*/ null, /*SUM*/ null, /*AVG*/ null, /*VIEW*/ null, /*AS*/ null},
			//VALS
			{/*CREATE*/ null, /*TABLE*/ null, /*INDEX*/ null, /*ID*/ null, /*INSERT*/ null, /*CONSVAL*/ Rule.R15, /*UPDATE*/ null, /*DELETE*/ null, /*SELECT*/ null, /*INT*/ null, /*VARCHAR*/ null, /*FROM*/ null, /*WHERE*/ null, /*ON*/ null, /*SET*/ null, /*INTO*/ null, /*VALUES*/ null, /*TRUE*/ null, /*FALSE*/ null, /*NOT*/ null, /*AND*/ null, /*OR*/ null, /*COMMA*/ null, /*LPARENTH*/ null, /*RPARENTH*/ null, /*EQ*/ null, /*GT*/ null, /*LT*/ null, /*GOE*/ null, /*LOE*/ null, /*ADD*/ null, /*SUB*/ null, /*MUL*/ null, /*DIV*/ null, /*SEMICOLON*/ null,
					/*PRIMARY*/ null, /*FOREIGN*/ null, /*KEY*/ null, /*REFERENCES*/ null, /*JOIN*/ null,
					/*GROUP*/ null, /*BY*/ null, /*HAVING*/ null, /*MAX*/ null, /*MIN*/ null, /*SUM*/ null, /*AVG*/ null, /*VIEW*/ null, /*AS*/ null},
			//VALSprime
			{/*CREATE*/ null, /*TABLE*/ null, /*INDEX*/ null, /*ID*/ null, /*INSERT*/ null, /*CONSVAL*/ null, /*UPDATE*/ null, /*DELETE*/ null, /*SELECT*/ null, /*INT*/ null, /*VARCHAR*/ null, /*FROM*/ null, /*WHERE*/ null, /*ON*/ null, /*SET*/ null, /*INTO*/ null, /*VALUES*/ null, /*TRUE*/ null, /*FALSE*/ null, /*NOT*/ null, /*AND*/ null, /*OR*/ null, /*COMMA*/ Rule.R16, /*LPARENTH*/ null, /*RPARENTH*/ Rule.R17, /*EQ*/ null, /*GT*/ null, /*LT*/ null, /*GOE*/ null, /*LOE*/ null, /*ADD*/ null, /*SUB*/ null, /*MUL*/ null, /*DIV*/ null, /*SEMICOLON*/ null,
					/*PRIMARY*/ null, /*FOREIGN*/ null, /*KEY*/ null, /*REFERENCES*/ null, /*JOIN*/ null,
					/*GROUP*/ null, /*BY*/ null, /*HAVING*/ null, /*MAX*/ null, /*MIN*/ null, /*SUM*/ null, /*AVG*/ null, /*VIEW*/ null, /*AS*/ null},
			//IDS
			{/*CREATE*/ null, /*TABLE*/ null, /*INDEX*/ null, /*ID*/ Rule.R21, /*INSERT*/ null, /*CONSVAL*/ null, /*UPDATE*/ null, /*DELETE*/ null, /*SELECT*/ null, /*INT*/ null, /*VARCHAR*/ null, /*FROM*/ null, /*WHERE*/ null, /*ON*/ null, /*SET*/ null, /*INTO*/ null, /*VALUES*/ null, /*TRUE*/ null, /*FALSE*/ null, /*NOT*/ null, /*AND*/ null, /*OR*/ null, /*COMMA*/ null, /*LPARENTH*/ null, /*RPARENTH*/ null, /*EQ*/ null, /*GT*/ null, /*LT*/ null, /*GOE*/ null, /*LOE*/ null, /*ADD*/ null, /*SUB*/ null, /*MUL*/ null, /*DIV*/ null, /*SEMICOLON*/ null,
					/*PRIMARY*/ null, /*FOREIGN*/ null, /*KEY*/ null, /*REFERENCES*/ null, /*JOIN*/ null,
					/*GROUP*/ null, /*BY*/ null, /*HAVING*/ null, /*MAX*/ null, /*MIN*/ null, /*SUM*/ null, /*AVG*/ null, /*VIEW*/ null, /*AS*/ null},
			//IDSprime
			{/*CREATE*/ null, /*TABLE*/ null, /*INDEX*/ null, /*ID*/ null, /*INSERT*/ null, /*CONSVAL*/ null, /*UPDATE*/ null, /*DELETE*/ null, /*SELECT*/ null, /*INT*/ null, /*VARCHAR*/ null, /*FROM*/ Rule.R23, /*WHERE*/ null, /*ON*/ null, /*SET*/ null, /*INTO*/ null, /*VALUES*/ null, /*TRUE*/ null, /*FALSE*/ null, /*NOT*/ null, /*AND*/ null, /*OR*/ null, /*COMMA*/ Rule.R22, /*LPARENTH*/ null, /*RPARENTH*/ null, /*EQ*/ null, /*GT*/ null, /*LT*/ null, /*GOE*/ null, /*LOE*/ null, /*ADD*/ null, /*SUB*/ null, /*MUL*/ null, /*DIV*/ null, /*SEMICOLON*/ Rule.R23,
					/*PRIMARY*/ null, /*FOREIGN*/ null, /*KEY*/ null, /*REFERENCES*/ null, /*JOIN*/ null,
					/*GROUP*/ null, /*BY*/ null, /*HAVING*/ Rule.R23, /*MAX*/ null, /*MIN*/ null, /*SUM*/ null, /*AVG*/ null, /*VIEW*/ null, /*AS*/ null},
			//TC
			{/*CREATE*/ null, /*TABLE*/ null, /*INDEX*/ null, /*ID*/ Rule.R26, /*INSERT*/ null, /*CONSVAL*/ Rule.R26, /*UPDATE*/ null, /*DELETE*/ null, /*SELECT*/ null, /*INT*/ null, /*VARCHAR*/ null, /*FROM*/ null, /*WHERE*/ null, /*ON*/ null, /*SET*/ null, /*INTO*/ null, /*VALUES*/ null, /*TRUE*/ Rule.R24, /*FALSE*/ Rule.R25, /*NOT*/ Rule.R28, /*AND*/ null, /*OR*/ null, /*COMMA*/ null, /*LPARENTH*/ Rule.R27, /*RPARENTH*/ null, /*EQ*/ null, /*GT*/ null, /*LT*/ null, /*GOE*/ null, /*LOE*/ null, /*ADD*/ null, /*SUB*/ null, /*MUL*/ null, /*DIV*/ null, /*SEMICOLON*/ null,
					/*PRIMARY*/ null, /*FOREIGN*/ null, /*KEY*/ null, /*REFERENCES*/ null, /*JOIN*/ null,
					/*GROUP*/ null, /*BY*/ null, /*HAVING*/ null, /*MAX*/ Rule.R26, /*MIN*/ Rule.R26, /*SUM*/ Rule.R26, /*AVG*/ Rule.R26, /*VIEW*/ null, /*AS*/ null},
			//CMPCV
			{/*CREATE*/ null, /*TABLE*/ null, /*INDEX*/ null, /*ID*/ null, /*INSERT*/ null, /*CONSVAL*/ null, /*UPDATE*/ null, /*DELETE*/ null, /*SELECT*/ null, /*INT*/ null, /*VARCHAR*/ null, /*FROM*/ null, /*WHERE*/ null, /*ON*/ null, /*SET*/ null, /*INTO*/ null, /*VALUES*/ null, /*TRUE*/ null, /*FALSE*/ null, /*NOT*/ null, /*AND*/ null, /*OR*/ null, /*COMMA*/ null, /*LPARENTH*/ null, /*RPARENTH*/ null, /*EQ*/ Rule.R29, /*GT*/ Rule.R30, /*LT*/ Rule.R31, /*GOE*/ Rule.R32, /*LOE*/ Rule.R33, /*ADD*/ null, /*SUB*/ null, /*MUL*/ null, /*DIV*/ null, /*SEMICOLON*/ null,
					/*PRIMARY*/ null, /*FOREIGN*/ null, /*KEY*/ null, /*REFERENCES*/ null, /*JOIN*/ null,
					/*GROUP*/ null, /*BY*/ null, /*HAVING*/ null, /*MAX*/ null, /*MIN*/ null, /*SUM*/ null, /*AVG*/ null, /*VIEW*/ null, /*AS*/ null},
			//OP
			{/*CREATE*/ null, /*TABLE*/ null, /*INDEX*/ null, /*ID*/ null, /*INSERT*/ null, /*CONSVAL*/ null, /*UPDATE*/ null, /*DELETE*/ null, /*SELECT*/ null, /*INT*/ null, /*VARCHAR*/ null, /*FROM*/ null, /*WHERE*/ null, /*ON*/ null, /*SET*/ null, /*INTO*/ null, /*VALUES*/ null, /*TRUE*/ null, /*FALSE*/ null, /*NOT*/ null, /*AND*/ Rule.R34, /*OR*/ Rule.R35, /*COMMA*/ null, /*LPARENTH*/ null, /*RPARENTH*/ null, /*EQ*/ null, /*GT*/ null, /*LT*/ null, /*GOE*/ null, /*LOE*/ null, /*ADD*/ null, /*SUB*/ null, /*MUL*/ null, /*DIV*/ null, /*SEMICOLON*/ null,
					/*PRIMARY*/ null, /*FOREIGN*/ null, /*KEY*/ null, /*REFERENCES*/ null, /*JOIN*/ null,
					/*GROUP*/ null, /*BY*/ null, /*HAVING*/ null, /*MAX*/ null, /*MIN*/ null, /*SUM*/ null, /*AVG*/ null, /*VIEW*/ null, /*AS*/ null},
			//CV
			{/*CREATE*/ null, /*TABLE*/ null, /*INDEX*/ null, /*ID*/ Rule.R37, /*INSERT*/ null, /*CONSVAL*/ Rule.R36, /*UPDATE*/ null, /*DELETE*/ null, /*SELECT*/ null, /*INT*/ null, /*VARCHAR*/ null, /*FROM*/ null, /*WHERE*/ null, /*ON*/ null, /*SET*/ null, /*INTO*/ null, /*VALUES*/ null, /*TRUE*/ null, /*FALSE*/ null, /*NOT*/ null, /*AND*/ null, /*OR*/ null, /*COMMA*/ null, /*LPARENTH*/ null, /*RPARENTH*/ null, /*EQ*/ null, /*GT*/ null, /*LT*/ null, /*GOE*/ null, /*LOE*/ null, /*ADD*/ null, /*SUB*/ null, /*MUL*/ null, /*DIV*/ null, /*SEMICOLON*/ null,
					/*PRIMARY*/ null, /*FOREIGN*/ null, /*KEY*/ null, /*REFERENCES*/ null, /*JOIN*/ null,
					/*GROUP*/ null, /*BY*/ null, /*HAVING*/ null, /*MAX*/ Rule.R55, /*MIN*/ Rule.R56, /*SUM*/ Rule.R57, /*AVG*/ Rule.R58, /*VIEW*/ null, /*AS*/ null},
			//CVprime
			{/*CREATE*/ null, /*TABLE*/ null, /*INDEX*/ null, /*ID*/ null, /*INSERT*/ null, /*CONSVAL*/ null, /*UPDATE*/ null, /*DELETE*/ null, /*SELECT*/ null, /*INT*/ null, /*VARCHAR*/ null, /*FROM*/ null, /*WHERE*/ Rule.R42, /*ON*/ null, /*SET*/ null, /*INTO*/ null, /*VALUES*/ null, /*TRUE*/ null, /*FALSE*/ null, /*NOT*/ null, /*AND*/ null, /*OR*/ null, /*COMMA*/ null, /*LPARENTH*/ null, /*RPARENTH*/ Rule.R42, /*EQ*/ Rule.R42, /*GT*/ Rule.R42, /*LT*/ Rule.R42, /*GOE*/ Rule.R42, /*LOE*/ Rule.R42, /*ADD*/ Rule.R38, /*SUB*/ Rule.R39, /*MUL*/ Rule.R40, /*DIV*/ Rule.R41, /*SEMICOLON*/ Rule.R42,
					/*PRIMARY*/ null, /*FOREIGN*/ null, /*KEY*/ null, /*REFERENCES*/ null, /*JOIN*/ null,
					/*GROUP*/ Rule.R42, /*BY*/ null, /*HAVING*/ null, /*MAX*/ null, /*MIN*/ null, /*SUM*/ null, /*AVG*/ null, /*VIEW*/ null, /*AS*/ null},
			//DT
			{/*CREATE*/ null, /*TABLE*/ null, /*INDEX*/ null, /*ID*/ null, /*INSERT*/ null, /*CONSVAL*/ null, /*UPDATE*/ null, /*DELETE*/ null, /*SELECT*/ null, /*INT*/ Rule.R12, /*VARCHAR*/ Rule.R13, /*FROM*/ null, /*WHERE*/ null, /*ON*/ null, /*SET*/ null, /*INTO*/ null, /*VALUES*/ null, /*TRUE*/ null, /*FALSE*/ null, /*NOT*/ null, /*AND*/ null, /*OR*/ null, /*COMMA*/ null, /*LPARENTH*/ null, /*RPARENTH*/ null, /*EQ*/ null, /*GT*/ null, /*LT*/ null, /*GOE*/ null, /*LOE*/ null, /*ADD*/ null, /*SUB*/ null, /*MUL*/ null, /*DIV*/ null, /*SEMICOLON*/ null,
					/*PRIMARY*/ null, /*FOREIGN*/ null, /*KEY*/ null, /*REFERENCES*/ null, /*JOIN*/ null,
					/*GROUP*/ null, /*BY*/ null, /*HAVING*/ null, /*MAX*/ null, /*MIN*/ null, /*SUM*/ null, /*AVG*/ null, /*VIEW*/ null, /*AS*/ null},
			//PK
			{/*CREATE*/ null, /*TABLE*/ null, /*INDEX*/ null, /*ID*/ null, /*INSERT*/ null, /*CONSVAL*/ null, /*UPDATE*/ null, /*DELETE*/ null, /*SELECT*/ null, /*INT*/ null, /*VARCHAR*/ null, /*FROM*/ null, /*WHERE*/ null, /*ON*/ null, /*SET*/ null, /*INTO*/ null, /*VALUES*/ null, /*TRUE*/ null, /*FALSE*/ null, /*NOT*/ null, /*AND*/ null, /*OR*/ null, /*COMMA*/ null, /*LPARENTH*/ null, /*RPARENTH*/ null, /*EQ*/ null, /*GT*/ null, /*LT*/ null, /*GOE*/ null, /*LOE*/ null, /*ADD*/ null, /*SUB*/ null, /*MUL*/ null, /*DIV*/ null, /*SEMICOLON*/ Rule.R44,
					/*PRIMARY*/ Rule.R43, /*FOREIGN*/ Rule.R44, /*KEY*/ null, /*REFERENCES*/ null, /*JOIN*/ null,
					/*GROUP*/ null, /*BY*/ null, /*HAVING*/ null, /*MAX*/ null, /*MIN*/ null, /*SUM*/ null, /*AVG*/ null, /*VIEW*/ null, /*AS*/ null},
			//FK
			{/*CREATE*/ null, /*TABLE*/ null, /*INDEX*/ null, /*ID*/ null, /*INSERT*/ null, /*CONSVAL*/ null, /*UPDATE*/ null, /*DELETE*/ null, /*SELECT*/ null, /*INT*/ null, /*VARCHAR*/ null, /*FROM*/ null, /*WHERE*/ null, /*ON*/ null, /*SET*/ null, /*INTO*/ null, /*VALUES*/ null, /*TRUE*/ null, /*FALSE*/ null, /*NOT*/ null, /*AND*/ null, /*OR*/ null, /*COMMA*/ null, /*LPARENTH*/ null, /*RPARENTH*/ null, /*EQ*/ null, /*GT*/ null, /*LT*/ null, /*GOE*/ null, /*LOE*/ null, /*ADD*/ null, /*SUB*/ null, /*MUL*/ null, /*DIV*/ null, /*SEMICOLON*/ Rule.R46,
					/*PRIMARY*/ null, /*FOREIGN*/ Rule.R45, /*KEY*/ null, /*REFERENCES*/ null, /*JOIN*/ null,
					/*GROUP*/ null, /*BY*/ null, /*HAVING*/ null, /*MAX*/ null, /*MIN*/ null, /*SUM*/ null, /*AVG*/ null, /*VIEW*/ null, /*AS*/ null},
			//TEX
			{/*CREATE*/ null, /*TABLE*/ null, /*INDEX*/ null, /*ID*/ Rule.R47, /*INSERT*/ null, /*CONSVAL*/ null, /*UPDATE*/ null, /*DELETE*/ null, /*SELECT*/ null, /*INT*/ null, /*VARCHAR*/ null, /*FROM*/ null, /*WHERE*/ null, /*ON*/ null, /*SET*/ null, /*INTO*/ null, /*VALUES*/ null, /*TRUE*/ null, /*FALSE*/ null, /*NOT*/ null, /*AND*/ null, /*OR*/ null, /*COMMA*/ null, /*LPARENTH*/ null, /*RPARENTH*/ null, /*EQ*/ null, /*GT*/ null, /*LT*/ null, /*GOE*/ null, /*LOE*/ null, /*ADD*/ null, /*SUB*/ null, /*MUL*/ null, /*DIV*/ null, /*SEMICOLON*/ null,
					/*PRIMARY*/ null, /*FOREIGN*/ null, /*KEY*/ null, /*REFERENCES*/ null, /*JOIN*/ null,
					/*GROUP*/ null, /*BY*/ null, /*HAVING*/ null, /*MAX*/ null, /*MIN*/ null, /*SUM*/ null, /*AVG*/ null, /*VIEW*/ null, /*AS*/ null},
			//TEXP
			{/*CREATE*/ null, /*TABLE*/ null, /*INDEX*/ null, /*ID*/ null, /*INSERT*/ null, /*CONSVAL*/ null, /*UPDATE*/ null, /*DELETE*/ null, /*SELECT*/ null, /*INT*/ null, /*VARCHAR*/ null, /*FROM*/ null, /*WHERE*/ Rule.R50, /*ON*/ null, /*SET*/ null, /*INTO*/ null, /*VALUES*/ null, /*TRUE*/ null, /*FALSE*/ null, /*NOT*/ null, /*AND*/ null, /*OR*/ null, /*COMMA*/ Rule.R49, /*LPARENTH*/ null, /*RPARENTH*/ null, /*EQ*/ null, /*GT*/ null, /*LT*/ null, /*GOE*/ null, /*LOE*/ null, /*ADD*/ null, /*SUB*/ null, /*MUL*/ null, /*DIV*/ null, /*SEMICOLON*/ null,
					/*PRIMARY*/ null, /*FOREIGN*/ null, /*KEY*/ null, /*REFERENCES*/ null, /*JOIN*/ Rule.R48,
					/*GROUP*/ null, /*BY*/ null, /*HAVING*/ null, /*MAX*/ null, /*MIN*/ null, /*SUM*/ null, /*AVG*/ null, /*VIEW*/ null, /*AS*/ null},
			//GB
			{/*CREATE*/ null, /*TABLE*/ null, /*INDEX*/ null, /*ID*/ null, /*INSERT*/ null, /*CONSVAL*/ null, /*UPDATE*/ null, /*DELETE*/ null, /*SELECT*/ null, /*INT*/ null, /*VARCHAR*/ null, /*FROM*/ null, /*WHERE*/ null, /*ON*/ null, /*SET*/ null, /*INTO*/ null, /*VALUES*/ null, /*TRUE*/ null, /*FALSE*/ null, /*NOT*/ null, /*AND*/ null, /*OR*/ null, /*COMMA*/ null, /*LPARENTH*/ null, /*RPARENTH*/ null, /*EQ*/ null, /*GT*/ null, /*LT*/ null, /*GOE*/ null, /*LOE*/ null, /*ADD*/ null, /*SUB*/ null, /*MUL*/ null, /*DIV*/ null, /*SEMICOLON*/ Rule.R51,
					/*PRIMARY*/ null, /*FOREIGN*/ null, /*KEY*/ null, /*REFERENCES*/ null, /*JOIN*/ null,
					/*GROUP*/ Rule.R52, /*BY*/ null, /*HAVING*/ null, /*MAX*/ null, /*MIN*/ null, /*SUM*/ null, /*AVG*/ null, /*VIEW*/ null, /*AS*/ null},
			//HV
			{/*CREATE*/ null, /*TABLE*/ null, /*INDEX*/ null, /*ID*/ null, /*INSERT*/ null, /*CONSVAL*/ null, /*UPDATE*/ null, /*DELETE*/ null, /*SELECT*/ null, /*INT*/ null, /*VARCHAR*/ null, /*FROM*/ null, /*WHERE*/ null, /*ON*/ null, /*SET*/ null, /*INTO*/ null, /*VALUES*/ null, /*TRUE*/ null, /*FALSE*/ null, /*NOT*/ null, /*AND*/ null, /*OR*/ null, /*COMMA*/ null, /*LPARENTH*/ null, /*RPARENTH*/ null, /*EQ*/ null, /*GT*/ null, /*LT*/ null, /*GOE*/ null, /*LOE*/ null, /*ADD*/ null, /*SUB*/ null, /*MUL*/ null, /*DIV*/ null, /*SEMICOLON*/ Rule.R53,
					/*PRIMARY*/ null, /*FOREIGN*/ null, /*KEY*/ null, /*REFERENCES*/ null, /*JOIN*/ null,
					/*GROUP*/ null, /*BY*/ null, /*HAVING*/ Rule.R54, /*MAX*/ null, /*MIN*/ null, /*SUM*/ null, /*AVG*/ null, /*VIEW*/ null, /*AS*/ null}
	};

    public static Rule at(NonTerminal row, Terminal col) {
        return table[row.ordinal()][col.ordinal()];
    }
}
