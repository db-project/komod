package komod.interpretation.parser;

import komod.lang.Rule;

import java.util.ArrayList;
import java.util.List;

/**
 * Komod, a lightweight DBMS written in Java.
 * Copyright (C) 2015 Komod
 * <p>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * See LICENCE.txt for more details.
 */
public class ParseNode {
    private Rule rule;
    private String value;
    private List<ParseNode> children = new ArrayList<>();

    public void setRule(Rule rule) {
        this.rule = rule;
    }

    public Rule getRule() {
        return this.rule;
    }

    public void addChild(ParseNode child) {
        children.add(child);
    }

    public List<ParseNode> getChildren() {
        return this.children;
    }

    public ParseNode getChild(int index) {
        return children.get(index);
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }

    @Override
    public String toString() {
        String s = "";
        for (ParseNode c : children)
            s += c.toString().replace("\n", "\n\t") + ",\n\t";
        return String.format("[\n\tParseNode\n\tRule: %s\n\tValue: %s\n\tChildren:\n\t%s\n]", rule, value, s);
    }
}
