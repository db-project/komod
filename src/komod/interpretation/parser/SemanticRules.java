package komod.interpretation.parser;

import komod.db.DataType;
import komod.db.ForeignKey;
import komod.lang.Rule;
import komod.query.*;
import komod.query.computation.*;
import komod.query.condition.*;
import komod.query.tableexpression.*;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.function.Function;

/**
 * Komod, a lightweight DBMS written in Java.
 * Copyright (C) 2015 Hadi
 * <p>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * See LICENCE.txt for more details.
 */
public class SemanticRules {
    private static EnumMap<Rule, Function<ParseNode, Object>> semanticRules;

    static {
        semanticRules = new EnumMap<>(Rule.class);
        initializeSemanticRules();
    }

    private static void initializeSemanticRules() {
        semanticRules.put(Rule.R1, SemanticRules::applyRuleR1);
        semanticRules.put(Rule.R2, SemanticRules::applyRuleR2);
        semanticRules.put(Rule.R3, SemanticRules::applyRuleR3);
        semanticRules.put(Rule.R4, SemanticRules::applyRuleR4);
        semanticRules.put(Rule.R5, SemanticRules::applyRuleR5);
        semanticRules.put(Rule.R6, SemanticRules::applyRuleR6);
        semanticRules.put(Rule.R7, SemanticRules::applyRuleR7);
        semanticRules.put(Rule.R8, SemanticRules::applyRuleR8);
        semanticRules.put(Rule.R9, SemanticRules::applyRuleR9);
        semanticRules.put(Rule.R10, SemanticRules::applyRuleR10);
        semanticRules.put(Rule.R11, SemanticRules::applyRuleR11);
        semanticRules.put(Rule.R12, SemanticRules::applyRuleR12);
        semanticRules.put(Rule.R13, SemanticRules::applyRuleR13);
        semanticRules.put(Rule.R14, SemanticRules::applyRuleR14);
        semanticRules.put(Rule.R15, SemanticRules::applyRuleR15);
        semanticRules.put(Rule.R16, SemanticRules::applyRuleR16);
        semanticRules.put(Rule.R17, SemanticRules::applyRuleR17);
        semanticRules.put(Rule.R18, SemanticRules::applyRuleR18);
        semanticRules.put(Rule.R19, SemanticRules::applyRuleR19);
        semanticRules.put(Rule.R20, SemanticRules::applyRuleR20);
        semanticRules.put(Rule.R21, SemanticRules::applyRuleR21);
        semanticRules.put(Rule.R22, SemanticRules::applyRuleR22);
        semanticRules.put(Rule.R23, SemanticRules::applyRuleR23);
        semanticRules.put(Rule.R24, SemanticRules::applyRuleR24);
        semanticRules.put(Rule.R25, SemanticRules::applyRuleR25);
        semanticRules.put(Rule.R26, SemanticRules::applyRuleR26);
        semanticRules.put(Rule.R27, SemanticRules::applyRuleR27);
        semanticRules.put(Rule.R28, SemanticRules::applyRuleR28);
        semanticRules.put(Rule.R29, SemanticRules::applyRuleR29);
        semanticRules.put(Rule.R30, SemanticRules::applyRuleR30);
        semanticRules.put(Rule.R31, SemanticRules::applyRuleR31);
        semanticRules.put(Rule.R32, SemanticRules::applyRuleR32);
        semanticRules.put(Rule.R33, SemanticRules::applyRuleR33);
        semanticRules.put(Rule.R34, SemanticRules::applyRuleR34);
        semanticRules.put(Rule.R35, SemanticRules::applyRuleR35);
        semanticRules.put(Rule.R36, SemanticRules::applyRuleR36);
        semanticRules.put(Rule.R37, SemanticRules::applyRuleR37);
        semanticRules.put(Rule.R38, SemanticRules::applyRuleR38);
        semanticRules.put(Rule.R39, SemanticRules::applyRuleR39);
        semanticRules.put(Rule.R40, SemanticRules::applyRuleR40);
        semanticRules.put(Rule.R41, SemanticRules::applyRuleR41);
        semanticRules.put(Rule.R42, SemanticRules::applyRuleR42);
        semanticRules.put(Rule.R43, SemanticRules::applyRuleR43);
        semanticRules.put(Rule.R44, SemanticRules::applyRuleR44);
        semanticRules.put(Rule.R45, SemanticRules::applyRuleR45);
        semanticRules.put(Rule.R46, SemanticRules::applyRuleR46);
        semanticRules.put(Rule.R47, SemanticRules::applyRuleR47);
        semanticRules.put(Rule.R48, SemanticRules::applyRuleR48);
        semanticRules.put(Rule.R49, SemanticRules::applyRuleR49);
        semanticRules.put(Rule.R50, SemanticRules::applyRuleR50);
        semanticRules.put(Rule.R51, SemanticRules::applyRuleR51);
        semanticRules.put(Rule.R52, SemanticRules::applyRuleR52);
        semanticRules.put(Rule.R53, SemanticRules::applyRuleR53);
        semanticRules.put(Rule.R54, SemanticRules::applyRuleR54);
        semanticRules.put(Rule.R55, SemanticRules::applyRuleR55);
        semanticRules.put(Rule.R56, SemanticRules::applyRuleR56);
        semanticRules.put(Rule.R57, SemanticRules::applyRuleR57);
        semanticRules.put(Rule.R58, SemanticRules::applyRuleR58);
        semanticRules.put(Rule.R59, SemanticRules::applyRuleR59);
    }



    public static Object applySemanticRules(ParseNode parseNode) {
        return semanticRules.get(parseNode.getRule()).apply(parseNode);
    }

    private static Object applyRuleR1(ParseNode parseNode) {
        return applySemanticRules(parseNode.getChildren().get(0));
    }

    private static Object applyRuleR2(ParseNode parseNode) {
        return applySemanticRules(parseNode.getChildren().get(0));
    }

    private static Object applyRuleR3(ParseNode parseNode) {
        return applySemanticRules(parseNode.getChildren().get(0));
    }

    private static Object applyRuleR4(ParseNode parseNode) {
        return applySemanticRules(parseNode.getChildren().get(0));
    }

    private static Object applyRuleR5(ParseNode parseNode) {
        return applySemanticRules(parseNode.getChildren().get(0));
    }

    private static Object applyRuleR6(ParseNode parseNode) {
        return applySemanticRules(parseNode.getChildren().get(1));
    }

    private static Object applyRuleR7(ParseNode parseNode) {
        String id = parseNode.getChild(1).getValue();
        LinkedList<String>[] columnData = (LinkedList[]) applySemanticRules(parseNode.getChild(3));
        ArrayList<ForeignKey> foreignKeys = (ArrayList<ForeignKey>) applySemanticRules(parseNode.getChild(6));
        String primaryKey = (String) applySemanticRules(parseNode.getChild(5));
        int size = columnData[0].size();
        String[] columnNames = columnData[0].toArray(new String[size]);
        String[] columnTypes = columnData[1].toArray(new String[size]);
        return new CreateTable(id, columnNames, columnTypes,foreignKeys.toArray(new ForeignKey[0]),primaryKey);
    }

    private static Object applyRuleR8(ParseNode parseNode) {
        String indexName = parseNode.getChild(1).getValue();
        String tableName = parseNode.getChild(3).getValue();
        String columnName = parseNode.getChild(5).getValue();
        return new CreateIndex(indexName, tableName, columnName);
    }

    private static Object applyRuleR9(ParseNode parseNode) {
        String name = parseNode.getChild(0).getValue();
        String type = (String) applySemanticRules(parseNode.getChild(1));
        LinkedList[] remainingData = (LinkedList[]) applySemanticRules(parseNode.getChild(2));
        remainingData[0].addFirst(name);
        remainingData[1].addFirst(type);
        return remainingData;
    }

    private static Object applyRuleR10(ParseNode parseNode) {
        String name = parseNode.getChild(1).getValue();
        String type = (String) applySemanticRules(parseNode.getChild(2));
        LinkedList[] remainingData = (LinkedList[]) applySemanticRules(parseNode.getChild(3));
        remainingData[0].addFirst(name);
        remainingData[1].addFirst(type);
        return remainingData;
    }

    private static Object applyRuleR11(ParseNode parseNode) {
        LinkedList[] emptyLists = new LinkedList[2];
        emptyLists[0] = new LinkedList<>();
        emptyLists[1] = new LinkedList<>();
        return emptyLists;
    }

    private static Object applyRuleR12(ParseNode parseNode) {
        return DataType.INTEGER.getName();
    }

    private static Object applyRuleR13(ParseNode parseNode) {
        return DataType.VARCHAR.getName();
    }

    private static Object applyRuleR14(ParseNode parseNode) {
        String tableName = parseNode.getChild(2).getValue();
        LinkedList<String> valuesList = (LinkedList) applySemanticRules(parseNode.getChild(5));
        String[] values = valuesList.toArray(new String[valuesList.size()]);
        return new InsertInto(tableName, values);
    }

    private static Object applyRuleR15(ParseNode parseNode) {
        LinkedList valuesList = (LinkedList) applySemanticRules(parseNode.getChild(1));
        valuesList.addFirst(parseNode.getChild(0).getValue());
        return valuesList;
    }

    private static Object applyRuleR16(ParseNode parseNode) {
        LinkedList valuesList = (LinkedList) applySemanticRules(parseNode.getChild(2));
        valuesList.addFirst(parseNode.getChild(1).getValue());
        return valuesList;
    }

    private static Object applyRuleR17(ParseNode parseNode) {
        return new LinkedList();
    }

    private static Object applyRuleR18(ParseNode parseNode) {
        String tableName = parseNode.getChild(1).getValue();
        String columnName = parseNode.getChild(3).getValue();
        LinkedList list = (LinkedList) applySemanticRules(parseNode.getChild(5));
        Computable computeValue = cvToTree(list);
        TupleCondition tupleCondition = (TupleCondition) applySemanticRules(parseNode.getChild(7));
        return new UpdateTable(tableName, columnName, computeValue, tupleCondition);
    }

    private static Object applyRuleR19(ParseNode parseNode) {
        String tableName = parseNode.getChild(2).getValue();
        TupleCondition tupleCondition = (TupleCondition) applySemanticRules(parseNode.getChild(4));
        return new DeleteFrom(tableName, tupleCondition);
    }

    private static Object applyRuleR20(ParseNode parseNode) {
        LinkedList<String> columnList = (LinkedList) applySemanticRules(parseNode.getChild(1));
        String[] columnNames = columnList.toArray(new String[columnList.size()]);
        TableExpression tableExpression = (TableExpression) applySemanticRules(parseNode.getChild(3));
        TupleCondition tupleCondition = (TupleCondition) applySemanticRules(parseNode.getChild(5));
        Object[] groupByParameters = (Object[]) applySemanticRules(parseNode.getChild(6));
        String[] groupByColumns = (String[]) groupByParameters[0];
        TupleCondition groupByTupleCondition = (TupleCondition) groupByParameters[1];
        return new SelectFrom(columnNames, tableExpression, tupleCondition, groupByColumns, groupByTupleCondition);
    }

    private static Object applyRuleR21(ParseNode parseNode) {
        String name = parseNode.getChild(0).getValue();
        LinkedList columnNames = (LinkedList) applySemanticRules(parseNode.getChild(1));
        columnNames.addFirst(name);
        return columnNames;
    }

    private static Object applyRuleR22(ParseNode parseNode) {
        String name = parseNode.getChild(1).getValue();
        LinkedList columnNames = (LinkedList) applySemanticRules(parseNode.getChild(2));
        columnNames.addFirst(name);
        return columnNames;
    }

    private static Object applyRuleR23(ParseNode parseNode) {
        return new LinkedList();
    }

    private static Object applyRuleR24(ParseNode parseNode) {
        return new True();
    }

    private static Object applyRuleR25(ParseNode parseNode) {
        return new False();
    }

    private static Object applyRuleR26(ParseNode parseNode) {
        Computable left = cvToTree((LinkedList) applySemanticRules(parseNode.getChild(0)));
        CompareCondition c = (CompareCondition) applySemanticRules(parseNode.getChild(1));
        c.setLHS(left);
        return c;
    }

    private static Object applyRuleR27(ParseNode parseNode) {
        TupleCondition lt = (TupleCondition) applySemanticRules(parseNode.getChild(1));
        BinaryCondition c = (BinaryCondition) applySemanticRules(parseNode.getChild(3));
        TupleCondition rt = (TupleCondition) applySemanticRules(parseNode.getChild(5));
        c.setLHS(lt);
        c.setRHS(rt);
        return c;
    }

    private static Object applyRuleR28(ParseNode parseNode) {
        TupleCondition c = (TupleCondition) applySemanticRules(parseNode.getChild(1));
        return new Not(c);
    }

    private static Object applyRuleR29(ParseNode parseNode) {
        LinkedList list = (LinkedList) applySemanticRules(parseNode.getChild(1));
        return new Equal(null, cvToTree(list));
    }

    private static Object applyRuleR30(ParseNode parseNode) {
        LinkedList list = (LinkedList) applySemanticRules(parseNode.getChild(1));
        return new GreaterThan(null, cvToTree(list));
    }

    private static Object applyRuleR31(ParseNode parseNode) {
        LinkedList list = (LinkedList) applySemanticRules(parseNode.getChild(1));
        return new LessThan(null, cvToTree(list));
    }

    private static Object applyRuleR32(ParseNode parseNode) {
        LinkedList list = (LinkedList) applySemanticRules(parseNode.getChild(1));
        return new GreaterEqual(null, cvToTree(list));
    }

    private static Object applyRuleR33(ParseNode parseNode) {
        LinkedList list = (LinkedList) applySemanticRules(parseNode.getChild(1));
        return new LessEqual(null, cvToTree(list));
    }

    private static Object applyRuleR34(ParseNode parseNode) {
        return new And(null, null);
    }

    private static Object applyRuleR35(ParseNode parseNode) {
        return new Or(null, null);
    }

    private static Computable cvToTree(LinkedList list) {
//        System.out.println(list);
        Iterator i = list.iterator();
        Computable root = (Computable) i.next();
        while (i.hasNext()) {
            BinaryComputation bc = (BinaryComputation) i.next();
            Computable c = (Computable) i.next();
            bc.setLHS(root);
            bc.setRHS(c);
            root = bc;
        }
//        Computable root = null;
//        BinaryComputation parent = null;
//        while (i.hasNext()) {
//            Computable c = (Computable) i.next();
//            if (i.hasNext()) {
//                BinaryComputation bc = (BinaryComputation) i.next();
//                if (root == null) {
//                    root = bc;
//                }
//                if (parent != null) {
//                    parent.setLHS(c);
//                    parent.setRHS(bc);
//                } else {
//                    bc.setLHS(c);
//                }
//                parent = bc;
//            } else {
//                if (root == null) {
//                    root = c;
//                }
//                if (parent != null) {
//                    parent.setRHS(c);
//                }
//            }
//        }
        return root;
    }

    private static Object getPreciseValue(String s) {
        try {
            return Integer.parseInt(s);
        } catch (Exception ignored) {
            return s;
        }
    }

    private static Object applyRuleR36(ParseNode parseNode) {
        Object value = getPreciseValue(parseNode.getChild(0).getValue());
        LinkedList list = (LinkedList) applySemanticRules(parseNode.getChild(1));
        list.addFirst(new ConstantValue(value));
        return list;
    }

    private static Object applyRuleR37(ParseNode parseNode) {
        String id = parseNode.getChild(0).getValue();
        LinkedList list = (LinkedList) applySemanticRules(parseNode.getChild(1));
        list.addFirst(new FieldValue(id));
        return list;
    }

    private static Object applyRuleR38(ParseNode parseNode) {
        LinkedList list = (LinkedList) applySemanticRules(parseNode.getChild(1));
        list.addFirst(new Addition());
        return list;
    }

    private static Object applyRuleR39(ParseNode parseNode) {
        LinkedList list = (LinkedList) applySemanticRules(parseNode.getChild(1));
        list.addFirst(new Subtraction());
        return list;
    }

    private static Object applyRuleR40(ParseNode parseNode) {
        LinkedList list = (LinkedList) applySemanticRules(parseNode.getChild(1));
        list.addFirst(new Multiplication());
        return list;
    }

    private static Object applyRuleR41(ParseNode parseNode) {
        LinkedList list = (LinkedList) applySemanticRules(parseNode.getChild(1));
        list.addFirst(new Division());
        return list;
    }

    private static Object applyRuleR42(ParseNode parseNode) {
        return new LinkedList();
    }

    private static Object applyRuleR43(ParseNode parseNode) {
        return parseNode.getChild(2).getValue();
    }

    private static Object applyRuleR44(ParseNode parseNode) {
        return null;
    }

    private static Object applyRuleR45(ParseNode parseNode) {
        ArrayList<ForeignKey> foreignKeys = (ArrayList<ForeignKey>) applySemanticRules(parseNode.getChild(11));
        ForeignKey foreignKey = new ForeignKey(parseNode.getChild(2).getValue(),
                                                parseNode.getChild(4).getValue(),
                                                null,
                                                parseNode.getChild(7).getValue().toUpperCase(),
                                                parseNode.getChild(10).getValue().toUpperCase());
        foreignKeys.add(foreignKey);
        return foreignKeys;
    }

    private static Object applyRuleR46(ParseNode parseNode) {
        return new ArrayList<ForeignKey>();
    }

    private static Object applyRuleR47(ParseNode parseNode) {
        Object object = applySemanticRules(parseNode.getChild(1));
        if (object == null){
            return new ConstantTableExpression(parseNode.getChild(0).getValue());
        } else {
            BinaryTableExpression binaryTableExp = (BinaryTableExpression) object;
            binaryTableExp.setLHS(new ConstantTableExpression(parseNode.getChild(0).getValue()));
            return binaryTableExp;
        }
    }

    private static Object applyRuleR48(ParseNode parseNode) {
        Join join = new Join();
        join.setRHS(new ConstantTableExpression(parseNode.getChild(1).getValue()));
        return join;
    }

    private static Object applyRuleR49(ParseNode parseNode) {
        CartessianProduct cartessian = new CartessianProduct();
        cartessian.setRHS(new ConstantTableExpression(parseNode.getChild(1).getValue()));
        return cartessian;
    }

    private static Object applyRuleR50(ParseNode parseNode) {
        return null;
    }

    private static Object applyRuleR51(ParseNode parseNode) {
        return new Object[] {new String[0], null};
    }

    private static Object applyRuleR52(ParseNode parseNode) {
        String[] columnNames = ((LinkedList<String>) applySemanticRules(parseNode.getChild(2))).toArray(new String[0]);
        TupleCondition tupleCondition = (TupleCondition) applySemanticRules(parseNode.getChild(3));
        return new Object[] {columnNames, tupleCondition};
    }

    private static Object applyRuleR53(ParseNode parseNode) {
        return new True();
    }

    private static Object applyRuleR54(ParseNode parseNode) {
        return (TupleCondition) applySemanticRules(parseNode.getChild(1));
    }

    private static Object applyRuleR55(ParseNode parseNode) {
        LinkedList list = (LinkedList) applySemanticRules(parseNode.getChild(4));
        list.addFirst(new MaxAggregation(parseNode.getChild(2).getValue()));
        return list;
    }

    private static Object applyRuleR56(ParseNode parseNode) {
        LinkedList list = (LinkedList) applySemanticRules(parseNode.getChild(4));
        list.addFirst(new MinAggregation(parseNode.getChild(2).getValue()));
        return list;
    }

    private static Object applyRuleR57(ParseNode parseNode) {
        LinkedList list = (LinkedList) applySemanticRules(parseNode.getChild(4));
        list.addFirst(new SumAggregation(parseNode.getChild(2).getValue()));
        return list;
    }

    private static Object applyRuleR58(ParseNode parseNode) {
        LinkedList list = (LinkedList) applySemanticRules(parseNode.getChild(4));
        list.addFirst(new AvgAggregation(parseNode.getChild(2).getValue()));
        return list;
    }

    private static Object applyRuleR59(ParseNode parseNode) {
        String viewName = parseNode.getChild(1).getValue();
        SelectFrom selectQuery = (SelectFrom) applySemanticRules(parseNode.getChild(3));
        return new CreateView(viewName, selectQuery);
    }




}